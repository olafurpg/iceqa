/**
 * 
 */
package is.iclt.icenlp.facade;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

/**
 * Horribly implemented Facade for IceNER.
 * 
 * Writes input string to file, executes bar/iceNER/iceNER.sh script
 * and then returns output from output file.
 * 
 * Does not work in a Windows OS.  
 * @author Olafur Pall Geirsson
 * @version Jul 15, 2013
 */
@Deprecated
public class IceNERFacadeBash {
    Runtime m_Runtime = null;
    String m_FacadeDir;
    String m_ScriptFile;
    String m_FileDir;
    String m_GazetteList;

    
    public IceNERFacadeBash() {
        m_Runtime = Runtime.getRuntime();

        m_FacadeDir = "./iceNLPFacade/";
        m_ScriptFile = m_FacadeDir + "iceNER.sh";
        m_FileDir= m_FacadeDir + "files/";
        m_GazetteList = m_FacadeDir + "location.txt"; // TODO: Add more locations to Gazette list
    }


    /**
     * Write String a_Text to file under directory "./IceNLPFacade/files/"
     * @param a_Text Text to write to file
     * @return Name of written file: "./IceNLPFacade/files/input.txt"
     */
    private String writeTextToFile(String a_Text) {
        String l_Filename = m_FacadeDir + "input.txt"; 
        // Write a_Text to file.
        try {
            File l_File = new File(l_Filename);
            if (!l_File.exists()) {
                l_File.createNewFile();
        	}

            FileWriter l_FW = new FileWriter(l_File.getAbsoluteFile());
            BufferedWriter l_BW = new BufferedWriter(l_FW);
            l_BW.write(a_Text);
            l_BW.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    	
        return l_Filename;
    }

    
    /**
     * Read text from a given filename
     * @param a_File The file to be read
     * @return The contents of a_File
     */
    private String readTextFromFile(String a_File) {
        // TODO: Change output format of NameSearcher to word/TAG
        BufferedReader l_BR = null;
        StringBuilder l_SB = new StringBuilder();
        String l_Separator = "";
        try {
 
            String l_CurrentLine;
 
            l_BR = new BufferedReader(new FileReader(a_File));
 
            while ((l_CurrentLine = l_BR.readLine()) != null) {
                l_SB.append(l_Separator).append(l_CurrentLine);
        	}
 
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (l_BR != null)l_BR.close();
            } catch (IOException ex) {
                ex.printStackTrace();
        	}
        }
        return l_SB.toString();
    }

    /**
     * Execute a shell command
     * @param a_Command Command to execute
     * @return System output from executing a_Command
     */
    private String[] ExecuteCommand(String a_Command) {    	
        LinkedList<String> output = new LinkedList<String>();
        try {
            Process pr = m_Runtime.exec(a_Command);
            BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));
            String line = null;
            while ((line = input.readLine()) != null) {
                output.add(line);
        	}

        } catch (IOException e) {
            e.printStackTrace();
        }

        return output.toArray(new String[0]); 
    }
    
    /**
     * Main facade to tag a a String with IceNER.
     * @param a_Text Text to be tagged
     * @return Tagged text
     */
    public String tagTextWithIceNER(String a_Text) {
    	
        String l_InputFile = writeTextToFile(a_Text);
        String l_OutputFile = l_InputFile + ".out";
        // We'll ignore the output from the bash script.
        String[] l_OutputFromCommand = ExecuteCommand(
                m_ScriptFile +
                " -l " + m_GazetteList + 
                " -i " + l_InputFile + 
                " -o " + l_OutputFile);
//         Print output from command
//        for (String l_Output : l_OutputFromCommand) {
//            System.out.println(l_Output);
//        }
        String l_TaggedText = readTextFromFile(l_OutputFile);
        return l_TaggedText;

    } 
    
    /**
     *  Testing purposes
     * @param args
     */
    public static void main(String[] args) {
        IceNERFacadeBash f = new IceNERFacadeBash();
        String l_TextToTag = "Walter Benjamin (1892-1940) var einn merkasti og sérstæðasti hugsuður á sviði hugvísinda á Vesturlöndum á 20. öld. Höfundarverk hans er margþætt og fjölbreytilegt og hann fékkst í skrifum sínum við jafn ólík viðfangsefni og borgarfræði, kvikmyndir, söguspeki, ljósmyndatækni, bókasöfn, frímerki og jurtir svo dæmi séu nefnd. Segja má að Benjamin hafi ekki látið neinar birtingarmyndir eða afurðir nútímamenningar vera sér óviðkomandi. Höfundarverk hans reynist ekki síður margbrotið ef horft er til þeirra ólíku hugmyndakerfa sem leidd eru saman í skrifum hans, því hér er á ferðinni afar sérstæður bræðingur marxískrar þjóðfélagsgreiningar, gyðinglegrar dulhyggju, hugleiðinga um hversdagsmenningu, þýskrar hughyggjuhefðar og söguskoðunar á forsendum andrökhyggju.";
        String foo = "Eiríksjökull er hæsta fjall Vesturlands, 1675 m hátt. Árið 2008 í Peking í Kína var svo komið að karlalandsliðinu í handknattleik en þeir unnu til silfurverðlauna. Í liðinu voru: Alexander Petersson, Arnór Atlason, Ásgeir Örn Hallgrímsson, Björgvin Páll Gústavsson, Guðjón Valur Sigurðsson, Hreiðar Levy Guðmundsson, Ingimundur Ingimundarson, Logi Geirsson, Ólafur Stefánsson, Róbert Gunnarsson, Sigfús Sigurðsson, Snorri Steinn Guðjónsson, Sturla Ásgeirsson og Sverre Andreas Jakobsson. Árið 1956 voru leikarnir haldnir í Melbourne í Ástralíu en þá vann Vilhjálmur Einarsson silfurverðlaun í þrístökki. Næstur var Bjarni Friðriksson sem vann til bronsverðlauna í júdó í Los Angeles í Bandaríkjunum árið 1984. Árið 2000 í Sidney í Ástralíu vann Vala Flosadóttir til bronsverðlauna í stangarstökki.";
//        String l_Output = f.TagTextWithIceNER(l_TextToTag);
        System.out.println(f.tagTextWithIceNER(l_TextToTag));
        System.out.println(f.tagTextWithIceNER(foo));
    }


}