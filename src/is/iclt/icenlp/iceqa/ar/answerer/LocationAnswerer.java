/**
 * 
 */
package is.iclt.icenlp.iceqa.ar.answerer;

import is.iclt.icenlp.iceqa.commons.AnswererUtil;
import is.iclt.icenlp.iceqa.commons.IAnswerer;
import is.iclt.icenlp.iceqa.commons.Passage;
import is.iclt.icenlp.iceqa.commons.TagParser;
import sg.edu.nus.wing.qanus.stock.ar.AnswerCandidate;

/**
 * Answering module for location questions 
 * @author Ólafur Páll Geirsson
 * @version Aug 8, 2013
 *
 */
public class LocationAnswerer implements IAnswerer {

	/* (non-Javadoc)
	 * @see sg.edu.nus.wing.qanus.framework.commons.IRegisterableModule#GetModuleID()
	 */
	@Override
	public String GetModuleID() {
		return "LocationAnswerer";
	}

	/* (non-Javadoc)
	 * @see is.iclt.icenlp.iceqa.commons.IAnswerer#Classname()
	 */
	@Override
	public String Classname() {
		return LocationAnswerer.class.getName();
	}

	/* (non-Javadoc)
	 * @see is.iclt.icenlp.iceqa.commons.IAnswerer#Subscriptions()
	 */
	@Override
	public String[] Subscriptions() {
		return new String[] {
				"QCLOCATION"
		};
	}

	/* (non-Javadoc)
	 * @see is.iclt.icenlp.iceqa.commons.IAnswerer#GetQuery(java.util.Map)
	 */
	@Override
	public String GetQuery(Passage a_Question) {
		String l_POS = a_Question.get("POS");
		String l_LEMMA = a_Question.get("LEMMA");
		return TagParser.FilterOutLemma(l_POS, l_LEMMA, "^(s|n).*");
	}

	/* (non-Javadoc)
	 * @see is.iclt.icenlp.iceqa.commons.IAnswerer#GetFieldToQueryLucene()
	 */
	@Override
	public String GetFieldToQueryLucene() {
		return "TEXT-ANNOTATED-IceLemmald";
	}

	/* (non-Javadoc)
	 * @see is.iclt.icenlp.iceqa.commons.IAnswerer#GetFieldToExtractCandidates()
	 */
	@Override
	public String GetFieldToExtractCandidates() {
		return "TEXT-ANNOTATED-IceNER";
	}

	/* (non-Javadoc)
	 * @see is.iclt.icenlp.iceqa.commons.IAnswerer#ExtractCandidates(java.lang.String[])
	 */
	@Override
	public AnswerCandidate[] ExtractCandidates(Passage[] a_BestSentences) {
		return AnswererUtil.ExtractPatternForNER("LOCATION", a_BestSentences);
	}

}
