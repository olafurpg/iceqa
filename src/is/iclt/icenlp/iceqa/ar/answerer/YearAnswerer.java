/**
 * 
 */
package is.iclt.icenlp.iceqa.ar.answerer;

import is.iclt.icenlp.iceqa.commons.AnswererUtil;
import is.iclt.icenlp.iceqa.commons.IAnswerer;
import is.iclt.icenlp.iceqa.commons.Passage;
import is.iclt.icenlp.iceqa.commons.TagParser;
import sg.edu.nus.wing.qanus.stock.ar.AnswerCandidate;


/**
 * @author Ólafur Páll Geirsson
 * @version Aug 7, 2013
 *
 */
public class YearAnswerer implements IAnswerer {

	/* (non-Javadoc)
	 * @see sg.edu.nus.wing.qanus.framework.commons.IRegisterableModule#GetModuleID()
	 */
	@Override
	public String GetModuleID() {
		return "YearAnswerer";
	}

	/* (non-Javadoc)
	 * @see is.iclt.icenlp.iceqa.commons.IAnswerer#Classname()
	 */
	@Override
	public String Classname() {
		return YearAnswerer.class.getName();
	}

	/* (non-Javadoc)
	 * @see is.iclt.icenlp.iceqa.commons.IAnswerer#Subscriptions()
	 */
	@Override
	public String[] Subscriptions() {
		return new String[] {
				"QCYEAR"
		};
	}

	@Override
	public AnswerCandidate[] ExtractCandidates(Passage[] a_BestSentences) {
//		String[] l_Data = AnswererUtil.GetField(a_BestSentences, "LEMMA");
		return AnswererUtil.ExtractPattern("[0-9]{3,4}", a_BestSentences);
	}

	
	/* Extract nouns, verbs, foreign words, unanalyzed words, adjectives and numbers
	 * @see is.iclt.icenlp.iceqa.commons.IAnswerer#GetQuery(java.util.Map)
	 */
	@Override
	public String GetQuery(Passage a_Question) {
		String l_POS = a_Question.get("POS");
		String l_LEMMA = a_Question.get("LEMMA");

		return TagParser.FilterOutLemma(l_POS, l_LEMMA, "^[nsexlt].*");
	}

	@Override
	public String GetFieldToQueryLucene() {
		return "TEXT-ANNOTATED-IceLemmald";
	}

	@Override
	public String GetFieldToExtractCandidates() {
		return "TEXT";
	}

}
