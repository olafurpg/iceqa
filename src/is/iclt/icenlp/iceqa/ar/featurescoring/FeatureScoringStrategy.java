/**
 * 
 */
package is.iclt.icenlp.iceqa.ar.featurescoring;

import is.iclt.icenlp.iceqa.commons.AnswererFactory;
import is.iclt.icenlp.iceqa.commons.AnswererUtil;
import is.iclt.icenlp.iceqa.commons.IAnswerer;
import is.iclt.icenlp.iceqa.commons.Passage;
import is.iclt.icenlp.iceqa.commons.TagParser;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.ScoreDoc;

import sg.edu.nus.wing.qanus.framework.commons.DataItem;
import sg.edu.nus.wing.qanus.framework.commons.IAnalyzable;
import sg.edu.nus.wing.qanus.framework.commons.IStrategyModule;
import sg.edu.nus.wing.qanus.stock.ar.AnswerCandidate;
import sg.edu.nus.wing.qanus.stock.ar.LuceneInformationBaseQuerier;
import sg.edu.nus.wing.qanus.stock.ar.featurescoring.FeatureScorer;
import sg.edu.nus.wing.qanus.stock.ar.featurescoring.FeatureSearchTermCoverage;
import sg.edu.nus.wing.qanus.stock.ar.featurescoring.FeatureSearchTermProximity;

/**
 * @author Olafur Pall Geirsson
 * @version Jul 5, 2013
 * 
 */
public class FeatureScoringStrategy implements IStrategyModule, IAnalyzable {

    // Number of top NN documents to retrieve from Knowledge Base
    private final int RESULTS_TO_RETRIEVE = 3;
    private static final String TEXT_ANNOTATED = "TEXT-ANNOTATED-";
    private LuceneInformationBaseQuerier m_InformationBase;

    public FeatureScoringStrategy() {
        Logger.getLogger("QANUS").setLevel(Level.ALL);
        ConsoleHandler l_Handler = new ConsoleHandler();
        l_Handler.setFormatter(new SimpleFormatter());
        Logger.getLogger("QANUS").addHandler(l_Handler);
    }

    public FeatureScoringStrategy(File a_IBFolder) {
        this();
        m_InformationBase = new LuceneInformationBaseQuerier(a_IBFolder,
                RESULTS_TO_RETRIEVE);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * sg.edu.nus.wing.qanus.framework.commons.IRegisterableModule#GetModuleID()
     */
    @Override
    public String GetModuleID() {
        return "IceFeatureScoringStrategy";
    }

    /*
     * (non-Javadoc)
     * 
     * @see sg.edu.nus.wing.qanus.framework.commons.IAnalyzable#
     * GetAnalysisInfoForQuestion
     * (sg.edu.nus.wing.qanus.framework.commons.DataItem)
     */
    @Override
    public DataItem GetAnalysisInfoForQuestion(DataItem a_QuestionItem) {
        return GetAnswerForQuestion(a_QuestionItem, true);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * sg.edu.nus.wing.qanus.framework.commons.IStrategyModule#GetAnswerForQuestion
     * (sg.edu.nus.wing.qanus.framework.commons.DataItem)
     */

    public DataItem GetAnswerForQuestion(DataItem a_QuestionItem) {
        return GetAnswerForQuestion(a_QuestionItem, false);
    }

    public DataItem GetAnswerForQuestion(DataItem a_QuestionItem,
            boolean a_Analysis) {
        if (a_Analysis)
            System.out.println(a_QuestionItem.toXMLString());
        String l_QuestionType = a_QuestionItem.GetAttribute("type");
        /*
         * Extract information from a_QuestionItem
         */
        if (l_QuestionType.compareToIgnoreCase("FACTOID") != 0) {
            return null; // IceQA only answers factoid questions for the moment
        }
        String l_QuestionID = a_QuestionItem.GetAttribute("id");

        // Question classifier result, assumes single class
        String l_ExpectedAnswerType = "";
        DataItem[] l_QCItems = a_QuestionItem.GetFieldValues("Q-IceQC");
        if (l_QCItems != null) {
            l_ExpectedAnswerType = (l_QCItems[0].GetValue())[0];
        }

        // Get actual question string
        String l_QuestionText = "";
        DataItem[] l_QItems = a_QuestionItem.GetFieldValues("q");
        if (l_QItems != null) {
            l_QuestionText = (l_QItems[0].GetValue())[0];
        }

        // Retrieve POS annotation
        String l_QuestionPOS = "";
        DataItem[] l_QuestionPOSItems = a_QuestionItem
                .GetFieldValues("Q-IcePOS");
        if (l_QuestionPOSItems != null) {
            l_QuestionPOS = (l_QuestionPOSItems[0].GetValue())[0];
        }

        // Retrieve lemmatised annotations
        String l_QuestionLemma = "";
        DataItem[] l_QuestionLemmaItems = a_QuestionItem
                .GetFieldValues("Q-IceLemmald");
        if (l_QuestionLemmaItems != null) {
            l_QuestionLemma = (l_QuestionLemmaItems[0].GetValue())[0];
        }

        // If analysis is required, prepare the return items
        DataItem l_AnalysisResults = new DataItem("Analysis");
        l_AnalysisResults.AddAttribute("QID", l_QuestionID);

        if (a_Analysis)
            System.out.println("QUESTION_TEXT: " + l_QuestionText);
        // System.out.println("POS: " + l_QuestionPOS);
        // System.out.println("POS: " + l_QuestionLemma);
        // System.out.println("EXPECTED_ANSWER: " + l_ExpectedAnswerType);

        // Create passage from question
        Passage l_Question = new Passage(l_QuestionText, l_QuestionLemma,
                l_QuestionPOS, "");
        if (a_Analysis)
            System.out.println("Question " + l_Question);
        l_Question.put("QC", l_ExpectedAnswerType);

        String l_Field = "SUBSCRIPTION";

        // Main part of function, get answer.
        try {
            // Retrieve answerers who subscribed to our question
            // TODO: Should we join the queries into? It could perform faster
            ScoreDoc[][] l_RetrievedAnswerers = {
            // (ScoreDoc[]) m_InformationBase.SearchQuery(l_Field, "TEXT-" +
            // l_QuestionText),
            // (ScoreDoc[]) m_InformationBase.SearchQuery(l_Field, "POS-" +
            // l_QuestionPOS),
            // (ScoreDoc[]) m_InformationBase.SearchQuery(l_Field, "LEMMA-" +
            // l_QuestionLemma),
            (ScoreDoc[]) m_InformationBase.SearchQuery(l_Field, "QC"
                    + l_ExpectedAnswerType) };

            if (l_RetrievedAnswerers == null)
                throw new Exception("No search query returned");

            // Collect answerer names
            List<String> l_AnswererNames = new ArrayList<String>();
            for (ScoreDoc[] l_Results : l_RetrievedAnswerers) {
                for (ScoreDoc l_Result : l_Results) {
                    Document l_Doc = m_InformationBase.GetDoc(l_Result.doc);
                    l_AnswererNames.add(l_Doc.getValues("CLASSNAME")[0]);
                }
            }

            // Instantiate answerers
            LinkedList<IAnswerer> l_Answerers = new LinkedList<IAnswerer>();
            for (String l_Answerer : l_AnswererNames) {
                l_Answerers.add(AnswererFactory.GenerateAnswerer(l_Answerer));
            }
            if (a_Analysis)
                System.out.println("Nr. of answerers: " + l_Answerers.size());

            // Answer questions
            String l_Answer = "";
            String l_AnswerSource = "";
            for (IAnswerer l_Answerer : l_Answerers) {
                if (a_Analysis)
                    System.out
                            .println("AnwererID: " + l_Answerer.GetModuleID());
                // 1. Get Subject of the question, extract keywords
                String l_Query = l_Answerer.GetQuery(l_Question);
                if (a_Analysis)
                    System.out.printf("Lucene Query: %s\nLucene field:%s\n",
                            l_Query, l_Answerer.GetFieldToQueryLucene());
                ScoreDoc[] l_Results;
                l_Results = (ScoreDoc[]) m_InformationBase.SearchQuery(
                        l_Answerer.GetFieldToQueryLucene(), l_Query);
                // Iterate over the Documents in the Hits object
                FeatureScorer l_FScorer = new FeatureScorer();
                if (a_Analysis)
                    System.out.println("Nr. of retrieved docs: "
                            + l_Results.length);
                for (int i = 0; i < l_Results.length; i++) {
                    if (a_Analysis)
                        System.out.println("Doc: " + i);
                    ScoreDoc l_ScoreDoc = l_Results[i];
                    Document l_Doc = m_InformationBase.GetDoc(l_ScoreDoc.doc);
                    String l_Headline = l_Doc.get("Headline");
                    if (a_Analysis)
                        System.out.println("HEADLINE: " + l_Headline);

                    String[] l_ArrText = l_Doc.getValues(l_Answerer
                            .GetFieldToExtractCandidates());
                    for (int j = 0; j < l_ArrText.length; j++) {
                        String l_Sentence = l_ArrText[j];
                        l_FScorer.AddDocument(l_Sentence, j);
                    }

                    // Retrieve best passages
                    FeatureScorer.Pair[] l_BestPassages = l_FScorer
                            .RetrieveTopDocumentsPairs(l_Query, 5);
                    //
                    // Extract all data from Document into an array of passages
                    Passage[] l_BestData = new Passage[l_BestPassages.length];
                    String[] l_Text = l_Doc.getValues("TEXT");
                    String[] l_Lemma = l_Doc
                            .getValues("TEXT-ANNOTATED-IceLemmald");
                    String[] l_NER = l_Doc.getValues("TEXT-ANNOTATED-IceNER");
                    String[] l_POS = l_Doc.getValues("TEXT-ANNOTATED-IcePOS");
                    for (int j = 0; j < l_BestPassages.length; j++) {
                        int k = l_BestPassages[j].getIndex();
                        if (l_NER == null) {
                            l_BestData[j] = new Passage(l_Text[k], l_Lemma[k], l_POS[k], "No NER");
                        } else {
                            l_BestData[j] = new Passage(l_Text[k], l_Lemma[k], l_POS[k], l_NER[k]);
                        }
                    }
                    AnswerCandidate[] l_Candidates;
                    l_Candidates = l_Answerer.ExtractCandidates(l_BestData);

                    // Heuristics
                    FeatureSearchTermProximity l_FS_Proximity = new FeatureSearchTermProximity();
                    FeatureSearchTermCoverage l_FS_Coverage = new FeatureSearchTermCoverage();

                    // Find the highest scoring candidate
                    double l_BestScore = Double.NEGATIVE_INFINITY;
                    int l_CurrIndex = 0;
                    int l_NumCandidates = l_Candidates.length;

                    for (AnswerCandidate l_CandidateEntry : l_Candidates) {
                        String l_Candidate = l_CandidateEntry.GetAnswer();
                        String l_CandidateSource = l_CandidateEntry.getSource()
                                .TEXT();
                        if (a_Analysis)
                            System.out.println("Candidate: " + l_Candidate); // +
                                                                             // ", Source="
                                                                             // +
                                                                             // l_CandidateSource
                        // );

                        // Proximity score
                        String[] l_ProximityStrings = { l_Candidate, l_Query };
                        double l_ProximityScore = l_FS_Proximity.GetScore(
                                l_ProximityStrings, l_CandidateSource);

                        // Coverage score of target within passage
                        String[] l_CoverageStrings = { l_Query };
                        double l_CoverageScore = l_FS_Coverage.GetScore(
                                l_CoverageStrings, l_CandidateSource);
                        double l_SplitSentenceScore = 0;
                        String l_SentenceToSplit = l_CandidateEntry.getSource()
                                .LEMMA();
                        l_SentenceToSplit = TagParser
                                .ExtractTags(l_SentenceToSplit);
                        for (String l_SplitSentence : l_SentenceToSplit
                                .split(" \\. ")) {
                            // System.out.println("l_SplitSentence=" +
                            // l_SplitSentence + ", candidate=" + l_Candidate);
                            // TODO: Fix so that 500 does not fit 1500 or lewis
                            // fits fooolewisbaaar
                            if (l_SplitSentence.indexOf(l_Candidate) != -1) {
                                double l_Score = l_FS_Coverage.GetScore(
                                        l_CoverageStrings, l_SplitSentence);
                                l_SplitSentenceScore = (l_SplitSentenceScore < l_Score) ? l_Score
                                        : l_SplitSentenceScore;
                                // System.out.println("SplitSentenceScore=" +
                                // l_SplitSentenceScore
                                // + ", candidate=" + l_Candidate
                                // + ", splitsentence=" + l_SplitSentence
                                // + ", score=" + l_Score
                                // );
                            }
                        }

                        // Penalise if the answer candidate consists of repeated
                        // words compared to the question target
                        // So we multiply a -1 into the score
                        String[] l_CandidateStrings = { l_Candidate };
                        double l_RepeatedTermScore = -1
                                * l_FS_Coverage.GetScore(l_CandidateStrings,
                                        l_CandidateSource);

                        // Score derived from rank of source passage
                        double l_SentenceScore = (double) (l_NumCandidates - l_CurrIndex)
                                / l_NumCandidates;

                        // TODO: Missing sanity score
                        // Tally the score
                        double l_TotalScore = (0.6 * l_CoverageScore)
                                + (0.1 * l_SentenceScore)
                                + (0.8 * l_ProximityScore)
                                * (0.8 * l_SplitSentenceScore)
                                + (0.1 * l_RepeatedTermScore);

                        if (l_TotalScore > l_BestScore) {
                            l_BestScore = l_TotalScore;
                            l_Answer = AnswererUtil.properCase(l_Candidate);
                            l_AnswerSource = l_CandidateSource;
                            System.out.println("Scores, TS=" + l_TotalScore
                                    + ", CS=" + l_CoverageScore + ", SC="
                                    + l_SentenceScore + ", PS="
                                    + l_ProximityScore + ", RTS="
                                    + l_RepeatedTermScore + ", SSS="
                                    + l_SplitSentenceScore);
                        }
                    }
                    if (l_Answer != "")
                        break;
                }
            }

            // Build the data item to return as result of this function
            DataItem l_Result = new DataItem("Result");
            l_Result.AddAttribute("QID", l_QuestionID);
            l_Result.AddField("Answer", l_Answer);
            l_Result.AddField("Source", l_AnswerSource);
            l_Result.AddField("QuestionType", l_ExpectedAnswerType);
            l_Result.AddField("AnswerString", l_AnswerSource);

            System.out.println("The answer is " + l_Answer);
            return l_Result;
        } catch (Exception e) {
            Logger.getLogger("QANUS").logp(Level.WARNING,
                    FeatureScoringStrategy.class.getName(),
                    "GetAnswerForQuestion", "General exception", e);
        }
        return a_QuestionItem;
    }
}
