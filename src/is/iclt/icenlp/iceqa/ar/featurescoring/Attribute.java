/**
 * 
 */
package is.iclt.icenlp.iceqa.ar.featurescoring;

/**
 * @author Olafur Pall Geirsson
 *
 */
public class Attribute implements Comparable<Attribute> {

	private String attribute;
	/**
	 * 
	 */
	public Attribute() {
		attribute = "";
	}

	public Attribute(String a_Attribute) {
		attribute = a_Attribute;
	}
	
	
	
	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Attribute o) {
		return getAttribute().compareTo(o.getAttribute());
	}

}
