/**
 * 
 */
package is.iclt.icenlp.iceqa.textprocessing;

import is.iclt.icenlp.core.formald.tags.TaggedText;
import is.iclt.icenlp.core.lemmald.Lemmald;
import is.iclt.icenlp.formald.tags.Ice4TagFormat;

import java.util.LinkedList;

/**
 * Wrapper for IceLemmald, output is Ice4TagFormat 
 * 
 * @see is.iclt.icenlp.iceqa.formald.Ice4TagFormat#encode()
 * @author Olafur Pall Geirsson
 * @version Jul 5, 2013
 *
 */
public class IceLemmald extends IceNLPProcessor {

	/**
	 * 
	 */
	public IceLemmald() {
		super(); // Load tagger
	}

	@Override
	public String GetModuleID() {
		return "IceLemmald";
	}

	/* (non-Javadoc)
	 * @see is.iclt.icenlp.iceqa.textprocessing.IceNLPProcessor#GetModuleID()
	 */
	@Override
	public String[] ProcessText(String[] a_Sentences) {
		assert(icenlp != null);
		LinkedList<String> l_TaggedSentences = new LinkedList<String>();
		TaggedText tt;
		for (String sentence : a_Sentences) {
			tt = icenlp.tagText(sentence);
			Lemmald lemmald = Lemmald.newInstance();
			lemmald.lemmatizeTagged(tt);
			l_TaggedSentences.add(tt.toString(new Ice4TagFormat()));
		}
		
//		System.out.println("\nBEGIN DataItem Module: " + GetModuleID());
//		for (String taggedString : l_TaggedSentences) {
//			System.out.println("SENTENCE: " + taggedString);
//		}
//		System.out.println("END DataItem Module: " + GetModuleID());

		return l_TaggedSentences.toArray(new String[0]);
	}


}
