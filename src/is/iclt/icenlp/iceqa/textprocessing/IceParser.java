/**
 * 
 */
package is.iclt.icenlp.iceqa.textprocessing;

import java.util.LinkedList;

/**
 * @author Olafur Pall Geirsson
 *
 */
public class IceParser extends IceNLPProcessor {

	/* (non-Javadoc)
	 * @see is.iclt.icenlp.iceqa.textprocessing.IceNLPProcessor#GetModuleID()
	 */
	@Override
	public String GetModuleID() {
		return "IceParser";
	}

	/* (non-Javadoc)
	 * @see is.iclt.icenlp.iceqa.textprocessing.IceNLPProcessor#ProcessText(java.lang.String[])
	 */
	@Override
	public String[] ProcessText(String[] a_Sentences) {
		assert(icenlp != null);
		LinkedList<String> l_TaggedSentences = new LinkedList<String>();
		for (String sentence : a_Sentences) {
				l_TaggedSentences.add(icenlp.tagAndParseLines(sentence));
			}
		
//		System.out.println("\nBEGIN DataItem Module: " + GetModuleID());
//		for (String taggedString : l_TaggedSentences) {
//			System.out.println("SENTENCE: " + taggedString);
//		}
//		System.out.println("END DataItem Module: " + GetModuleID());

		return l_TaggedSentences.toArray(new String[0]);
	}

}
