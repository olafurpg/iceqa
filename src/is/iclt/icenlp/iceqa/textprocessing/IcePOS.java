/**
 * 
 */
package is.iclt.icenlp.iceqa.textprocessing;

import is.iclt.icenlp.core.formald.tags.TaggedText;
import is.iclt.icenlp.formald.tags.Ice3TagFormat;

import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Olafur Pall Geirsson
 *
 */
public class IcePOS extends IceNLPProcessor {

	/**
	 * 
	 */
	public IcePOS() {
		super(); // Load tagger
	}

	/* (non-Javadoc)
	 * @see is.ictl.icenlp.iceqa.textprocessing.IceNLPProcessor#GetModuleID()
	 */
	@Override
	public String GetModuleID() {
		return "IcePOS";
	}

	/* (non-Javadoc)
	 * @see is.ictl.icenlp.iceqa.textprocessing.IceNLPProcessor#ProcessText(java.lang.String[])
	 */
	@Override
	public String[] ProcessText(String[] a_Sentences) {
		assert(icenlp != null);
		LinkedList<String> l_TaggedSentences = new LinkedList<String>();
		TaggedText tt;
		for (String sentence : a_Sentences) {
			try {
				tt = icenlp.tagText(sentence);
				l_TaggedSentences.add(tt.toString(new Ice3TagFormat()));
			} catch (Exception e) {
				Logger.getLogger("QANUS").logp(Level.WARNING, IcePOS.class.getName(), "ProcessText", "Unable to tag sentence [" + sentence + "]");
				continue;
			}
		}
		
//		System.out.println("\nBEGIN DataItem Module: " + GetModuleID());
//		for (String taggedString : l_TaggedSentences) {
//			System.out.println("SENTENCE: " + taggedString);
//		}
//		System.out.println("END DataItem Module: " + GetModuleID());

		return l_TaggedSentences.toArray(new String[0]);
	}

}
