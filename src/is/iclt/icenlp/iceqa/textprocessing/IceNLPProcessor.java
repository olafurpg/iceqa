/**
 * 
 */
package is.iclt.icenlp.iceqa.textprocessing;

import is.iclt.icenlp.facade.IceNLP;

import java.util.logging.Level;
import java.util.logging.Logger;

import sg.edu.nus.wing.qanus.framework.commons.ITextProcessingModule;

/**
 * Superclass for processing IceNLP tools
 * @author Olafur Pall Geirsson
 *
 */
public abstract class IceNLPProcessor implements ITextProcessingModule {

	IceNLP icenlp;
	/**
	 * Load IceNLP instance
	 */
	public IceNLPProcessor() {
		// TODO: Do something if loading fails 
		LoadTagger(); 
	}
	
	private boolean LoadTagger() {
		try {
			icenlp = IceNLP.getInstance();			
		} catch (Exception e) {
			Logger.getLogger("QANUS").logp(Level.SEVERE, IceNLPProcessor.class.getName(), "LoadTagger", "Unable to load IceNLP through IceNLP.newInstance()");
			icenlp = null;
			return false;
		}
		
		return false;		
	}

	/* (non-Javadoc)
	 * @see sg.edu.nus.wing.qanus.framework.commons.IRegisterableModule#GetModuleID()
	 */
	public abstract String GetModuleID();

	/* (non-Javadoc)
	 * @see sg.edu.nus.wing.qanus.framework.commons.ITextProcessingModule#ProcessText(java.lang.String[])
	 */
	@Override
	public abstract String[] ProcessText(String[] arg0); // TODO: Implement this using function from interface

}
