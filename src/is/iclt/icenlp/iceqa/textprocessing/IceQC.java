/**
 * 
 */
package is.iclt.icenlp.iceqa.textprocessing;

import java.util.regex.Pattern;

/**
 * A simple question classifier using pattern matching.
 * 
 * Classifies question into 4 different types, PERSON, LOCATION, YEAR or UKNOWN.
 * @author Olafur Pall Geirsson
 * @version Jul 16, 2013
 *
 */
// TODO: Refactor into a generic java class for a QC Class
public class IceQC {

	Pattern[] m_PersonPatterns;
	Pattern[] m_LocationPatterns;
	Pattern[] m_YearPatterns;
	
	public IceQC() {		
		m_PersonPatterns = new Pattern[] {
				Pattern.compile("^eftir hvern.+"),
				Pattern.compile("^hver.+"),
				Pattern.compile("^hvaða.*ingur.+"),
				Pattern.compile("^hvaða .*maður.+"),
				Pattern.compile("^við hvern .+")
		};
		
		m_LocationPatterns = new Pattern[] {
				Pattern.compile("^hvar.+"),
				Pattern.compile("^hver er höfuðborg .+"),
				Pattern.compile("^hvaðan (kom|kemur).+"),
				Pattern.compile(".*hvaða lands.+"),
				Pattern.compile("^hvað er.*sta fljót.+"),
				Pattern.compile("^(í )?hvaða (land|borg).+"),
				Pattern.compile("^við hvaða háskóla.+")
		};
		m_YearPatterns = new Pattern[] {
				Pattern.compile("^hvaða ár.+"),
				Pattern.compile("^(síðan )?hvenær.+")
		};
		
	}
	
	private enum QuestionClass {UNKNOWN, PERSON, LOCATION, YEAR, NUMERIC};
	
	/* (non-Javadoc)
	 * @see sg.edu.nus.wing.qanus.framework.commons.IRegisterableModule#GetModuleID()
	 */
	
	public String GetClass(QuestionClass a_Class) {
		switch (a_Class) {
		case PERSON:
			return "PERSON";
		case LOCATION:
			return "LOCATION";
		case YEAR:
			return "YEAR";
		default:
			// TODO: Log error
			return "UNKNOWN";
		}
	}

	private boolean HasMatchingPattern(Pattern[] a_Patterns, String a_Text) {
		for (Pattern l_Pattern : a_Patterns) { 
			if (l_Pattern.matcher(a_Text).matches()) {
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * Classify a question
	 * @param a_Question
	 * @return
	 */
	public String Classify(String a_Question) {
		a_Question = a_Question.toLowerCase();
		QuestionClass l_ResultClass = QuestionClass.UNKNOWN;

		if (HasMatchingPattern(m_LocationPatterns, a_Question)) {
			l_ResultClass = QuestionClass.LOCATION;
		} else if (HasMatchingPattern(m_YearPatterns, a_Question)) {
			l_ResultClass = QuestionClass.YEAR;
		} else if (HasMatchingPattern(m_PersonPatterns, a_Question)) {
			l_ResultClass = QuestionClass.PERSON;
		} 
		
		return GetClass(l_ResultClass);
	}
}
