/**
 * 
 */
package is.iclt.icenlp.iceqa.textprocessing;

import is.iclt.icenlp.core.iceNER.NameSearcher;
import is.iclt.icenlp.facade.IceNERFacade;

import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

import sg.edu.nus.wing.qanus.framework.commons.ITextProcessingModule;

/**
 * @author Olafur Pall Geirsson
 * @version Jul 15, 2013
 *
 */
public class IceNER implements ITextProcessingModule {

    private IceNERFacade iceNER;
    
    public IceNER() {
        iceNER = new IceNERFacade();
    }
    
    /* (non-Javadoc)
     * @see sg.edu.nus.wing.qanus.framework.commons.IRegisterableModule#GetModuleID()
     */
    @Override
    public String GetModuleID() {
        iceNER = new IceNERFacade();
        return "IceNER";
    }

    /* (non-Javadoc)
     * @see sg.edu.nus.wing.qanus.framework.commons.ITextProcessingModule#ProcessText(java.lang.String[])
     */
    @Override
    public String[] ProcessText(String[] a_Sentences) {
        LinkedList<String> l_Text = new LinkedList<String>();
        // Join all sentences to process text with a single command
        for (String l_Sentence : a_Sentences) {
//            System.out.printf("input: %s\n", l_Sentence);
            String result;
            try {
                result = iceNER.recogniseText(l_Sentence);
                l_Text.add(result);
            } catch (ArrayIndexOutOfBoundsException e) {
                l_Text.add("No results from NER");
                // IceNER is buggy, we ignore any segfaults
                Logger.getLogger("QANUS").logp(Level.INFO, NameSearcher.class.getName(), "IceNER.ProcessText", e.getMessage());
            }
        }
        return l_Text.toArray(new String[0]);
    }
}
