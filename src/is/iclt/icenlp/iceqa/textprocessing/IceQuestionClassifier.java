/**
 * 
 */
package is.iclt.icenlp.iceqa.textprocessing;

import java.util.LinkedList;

import sg.edu.nus.wing.qanus.framework.commons.ITextProcessingModule;

/**
 * Simple Question classifier using pattern matching.
 * 
 * Classifies questions into two categories, PERSON and LOCATION.
 * 
 * @author Olafur Pall Geirsson
 * @version Jul 16, 2013
 *
 */
public class IceQuestionClassifier implements ITextProcessingModule {
	IceQC m_QC;
	public IceQuestionClassifier() {
		m_QC = new IceQC();
	}
	/* (non-Javadoc)
	 * @see sg.edu.nus.wing.qanus.framework.commons.IRegisterableModule#GetModuleID()
	 */
	@Override
	public String GetModuleID() {
		return "IceQC";
	}
	
	/* (non-Javadoc)
	 * @see sg.edu.nus.wing.qanus.framework.commons.ITextProcessingModule#ProcessText(java.lang.String[])
	 */
	@Override
	public String[] ProcessText(String[] a_Text) {
		LinkedList<String> l_ProcessesText = new LinkedList<String>();
		for (String l_Text : a_Text) {
			l_ProcessesText.add(m_QC.Classify(l_Text));
		}
		return l_ProcessesText.toArray(new String[0]);
	}

}
