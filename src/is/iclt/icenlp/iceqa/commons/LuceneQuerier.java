package is.iclt.icenlp.iceqa.commons;

/**
 * @author Ólafur Páll Geirsson
 * @version Aug 7, 2013
 *
 */
public class LuceneQuerier {
	private String m_Query;
	private String m_FieldToQuery;
	private String m_FieldToExtract;
	public LuceneQuerier(String m_Query, String m_FieldToQuery,
			String m_FieldToExtract) {
		super();
		this.m_Query = m_Query;
		this.m_FieldToQuery = m_FieldToQuery;
		this.m_FieldToExtract = m_FieldToExtract;
	}
	/**
	 * @return the m_Query
	 */
	public String getQuery() {
		return m_Query;
	}
	/**
	 * @return the m_FieldToQuery
	 */
	public String getFieldToQuery() {
		return m_FieldToQuery;
	}
	/**
	 * @return the m_FieldToExtract
	 */
	public String getFieldToExtract() {
		return m_FieldToExtract;
	}
	
	
	
	
}
