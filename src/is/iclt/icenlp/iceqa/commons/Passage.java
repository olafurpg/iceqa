/**
 * 
 */
package is.iclt.icenlp.iceqa.commons;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import edu.stanford.nlp.misc.SeeChars;

/**
 * A Passage including Lemma, POS, NER tagging 
 * 
 * Possible to add any other field
 * @author Ólafur Páll Geirsson
 * @version Aug 11, 2013
 *
 */
// A bit overkill to implement Map<String, String>, could have used inheritance
public class Passage implements Map<String, String> {
	
	Map<String, String> m_Passage;
	
	public Passage() {
		m_Passage = new HashMap<String, String>();
	}
	
	public Passage(String a_Text, String a_Lemma, String a_POS, String a_NER) {
		this();
		m_Passage.put("TEXT", a_Text);
		m_Passage.put("LEMMA", a_Lemma);
		m_Passage.put("POS", a_POS);
		m_Passage.put("NER", a_NER);
	}

	public String NER() {
		return m_Passage.get("NER");
	}
	
	public String TEXT() {
		return m_Passage.get("TEXT");
	}
	
	public String POS() {
		return m_Passage.get("POS");
	}
	
	public String LEMMA() {
		return m_Passage.get("LEMMA");
	}

	

	/* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Passage:\n" +
                "TEXT=" + TEXT() + "\n" +
                "POS=" + POS() + "\n" +
                "LEMMA=" + LEMMA() + "\n";
    }

    @Override
	public int size() {
		return m_Passage.size();
	}

	@Override
	public boolean isEmpty() {
		return m_Passage.isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return m_Passage.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return m_Passage.containsValue(value);
	}

	@Override
	public String get(Object key) {
		return m_Passage.get(key);
	}

	@Override
	public String put(String key, String value) {
		return m_Passage.put(key, value);
	}

	@Override
	public String remove(Object key) {
		return m_Passage.remove(key);
	}

	@Override
	public void putAll(Map<? extends String, ? extends String> m) {
		m_Passage.putAll(m);
	}

	@Override
	public void clear() {
		m_Passage.clear();
	}

	@Override
	public Set<String> keySet() {
		return m_Passage.keySet();
	}

	@Override
	public Collection<String> values() {
		return m_Passage.values();
	}

	@Override
	public Set<java.util.Map.Entry<String, String>> entrySet() {
		return m_Passage.entrySet();
	}
}
