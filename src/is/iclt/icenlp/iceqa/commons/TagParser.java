/**
 * 
 */
package is.iclt.icenlp.iceqa.commons;

import java.util.StringTokenizer;
import java.util.regex.Pattern;

/**
 * @author ollie
 * @version Jul 31, 2013
 *
 */
public final class TagParser {
	
	public static String ExtractTags(String a_TaggedString) {
		StringTokenizer l_ST = new StringTokenizer(a_TaggedString);
		StringBuilder l_SB = new StringBuilder();
		
		while (l_ST.hasMoreTokens()) {
			String[] l_Tag = l_ST.nextToken().split("/");
			if (l_Tag.length < 2) { // no tag
				return l_SB.toString();
			} else  { // tag matches filter
				l_SB.append(l_Tag[1]).append(" ");
			}
		}
		return l_SB.toString(); // .substring(0, l_Result.length() ); // cut out last space
	}
	
	public static String FilterOutLemma(String a_TaggedString, String a_LemmaString, String a_FilterTag) {
		String l_Words = TagParser.FilterTags(a_TaggedString, a_FilterTag);
		l_Words = "(" + l_Words.replace(" ", "|") + ")";
//		System.out.println("Words: " + l_Words);
		return TagParser.FilterWords(a_LemmaString, l_Words);
		
	}
	
	public static String FilterTags(String a_TaggedString, String a_FilterTag) {
		return FilterWordOrTag(a_TaggedString, a_FilterTag, true);
	}
	public static String FilterWords(String a_TaggedString, String a_FilterWord) {
		
		return FilterWordOrTag(a_TaggedString, a_FilterWord, false);
	}
	
	/**
	 * Filters out words or tags from a_TaggedString that match the regex pattern a_FilterTag
	 * @param a_TaggedString
	 * @param a_FilterTag
	 * @return Space separated string of words whose tag match the filter
	 * @param a_FilterTags True to filter out words, False to filter out tags
	 * @return
	 */
	public static String FilterWordOrTag(String a_TaggedString, String a_FilterTag, boolean a_FilterTags) {
		int l_FilterOut = (a_FilterTags) ? 0 : 1;
		Pattern l_TagPattern = Pattern.compile(a_FilterTag);
		StringTokenizer l_ST = new StringTokenizer(a_TaggedString);
		String l_Result = "";
		
		while (l_ST.hasMoreTokens()) {
		    String token = l_ST.nextToken();
//		    System.out.println("Token: " + token);
			String[] l_Tag = token.split("/");
			if (l_Tag.length < 2) { // no tag
				return l_Result;
			} else if (l_TagPattern.matcher(l_Tag[1 - l_FilterOut]).matches()) { // tag matches filter
				l_Result += l_Tag[l_FilterOut] + " ";
			}
		}
//		System.out.println("Result: " + l_Result);
		return l_Result;
	}
}
