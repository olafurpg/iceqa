/**
 * 
 */
package is.iclt.icenlp.iceqa.commons;

import is.iclt.icenlp.iceqa.ar.answerer.LocationAnswerer;
import is.iclt.icenlp.iceqa.ar.answerer.NameAnswerer;
import is.iclt.icenlp.iceqa.ar.answerer.YearAnswerer;

import java.io.File;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import sg.edu.nus.wing.qanus.framework.commons.DataItem;
import sg.edu.nus.wing.qanus.stock.ibp.InformationBaseWithLucene;

/**
 * @author Olafur Pall Geirsson
 * @version Jul 14, 2013
 *
 */
public class AnswererBuilder implements AnswererFramework {

	/* (non-Javadoc)
	 * @see is.iclt.icenlp.iceqa.commons.AnswererFramework#GetAnswerers()
	 */
	@Override
	public IAnswerer[] GetAnswerers() {
		IAnswerer[] l_Answerers = {
				new YearAnswerer(),
				new NameAnswerer(),
				new LocationAnswerer()
		};
		return l_Answerers;

	}

	/* 
	 * @see is.iclt.icenlp.iceqa.commons.AnswererFramework#LoadAnswerers()
	 */
	@Override
	public boolean LoadAnswerers(InformationBaseWithLucene a_Builder) {
		a_Builder.RemoveAllAnswerersFromInfoBase();
		IAnswerer[] l_Answerers = GetAnswerers();
		
		for (IAnswerer l_Answerer : l_Answerers) {
			DataItem l_AnswererItem = new DataItem("IAnswerer-" + l_Answerer.GetModuleID());
			l_AnswererItem.AddField("CLASSNAME", l_Answerer.Classname());
			String[] l_Subcriptions = l_Answerer.Subscriptions();
			for (String l_Subscription : l_Subcriptions) {
				l_AnswererItem.AddField("SUBSCRIPTION",	l_Subscription);
			}
			System.out.println(l_AnswererItem.toXMLString());
			if (!a_Builder.AddAnswererToInfoBase(l_AnswererItem)) {
                Logger.getLogger("QANUS").log(Level.ERROR, "Unable to remove Answerers from Lucene index");
				return false;
			}
		}
		a_Builder.CloseWriter();
		return true;

	}
	
	public static void main(String[] args) {
		File l_Index = new File("./index");
		InformationBaseWithLucene l_IB = new InformationBaseWithLucene(l_Index, false);
		AnswererBuilder l_AB = new AnswererBuilder();
		l_AB.LoadAnswerers(l_IB);
	}
}
