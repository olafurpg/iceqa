/**
 * 
 */
package is.iclt.icenlp.iceqa.commons;

import java.util.AbstractMap.SimpleEntry;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sg.edu.nus.wing.qanus.stock.ar.AnswerCandidate;


/**
 * General utility class for classes implementing IAnswerer interface
 * @author Ólafur Páll Geirsson
 * @version Aug 9, 2013
 *
 */
public final class AnswererUtil {
	public static String NER = "TEXT-ANNOTATED-IceNER";
	public static String Lemma = "TEXT-ANNOTATED-IceLemmald";
	public static String POS = "TEXT-ANNOTATED-IcePOS";
	
	
	/**
	 * Converts string to proper case, i.e. "hELLo" to "Hello"
	 * 
	 * Uses default locale
	 * @param a_Text
	 * @return
	 */
	public final static String properCase(String a_Text) {
		StringTokenizer l_ST = new StringTokenizer(a_Text);
		StringBuilder l_SB = new StringBuilder();
		while (l_ST.hasMoreTokens()) {
			String l_Token = l_ST.nextToken();
			String l_FirstChar = l_Token.substring(0, 1);
			l_SB.append(l_FirstChar.toUpperCase(Locale.getDefault()) + l_Token.substring(1));
			l_SB.append(" ");
		}
		return l_SB.toString().trim();
	}
	
	/**
	 * Extract a field from an array of passages 
	 * @param a_Passages Passages to extract text from
	 * @param a_Field Fieldname to extract
	 * @return Extracted field values
	 */
	public final static String[] GetField(Passage[] a_Passages, String a_Field) {
		String[] l_Text = new String[a_Passages.length];
		for (int i = 0; i < l_Text.length; i++) {
			l_Text[i] = a_Passages[i].get(a_Field);
		}
		return l_Text;
	}
	
	/**
	 * Extract a list of candidates from a NER passage
	 * @param a_Pattern Regular expression for the last word inside square brackets. 
	 * For example the pattern <code>"LOCATION"</code> will match all <code>X</code> in 
	 * inside a <code>[ X X X LOCATION]</code> NER string.
	 * 
	 * @param a_Text Passages to extract pattern from
	 * @return Extracted values, in lemmatised form
	 */
	public final static AnswerCandidate[] ExtractPatternForNER(String a_Pattern, Passage[] a_Text) {
		String l_LQ = Pattern.quote("["),
				l_RQ = Pattern.quote("]");
		Pattern l_Pattern = Pattern.compile(
		        "(?<=" + l_LQ + ")[^" + l_LQ + "]+?(?= " + a_Pattern + l_RQ + ")");
		Set<AnswerCandidate> l_Candidates = new TreeSet<AnswerCandidate>();
		for (int i = 0; i < a_Text.length; i++) {
			String l_NER = a_Text[i].NER();
			String l_Lemma = a_Text[i].LEMMA();
			Matcher l_Matches = l_Pattern.matcher(l_NER);
			while (l_Matches.find()) {
				String l_Name = l_Matches.group();
				String[] l_NameSplit = l_Name.split(" ");
				StringBuilder l_SB = new StringBuilder();
				for (String l_NamePart : l_NameSplit) {
					Pattern l_NamePattern = Pattern.compile("(?<=" + l_NamePart + "/)[^ ]+");
					Matcher l_NameLemmaMatcher = l_NamePattern.matcher(l_Lemma);
					if (l_NameLemmaMatcher.find()) {
						l_SB.append(l_NameLemmaMatcher.group()).append(" ");
					}
				}
				String l_NameLemma = l_SB.toString().trim();
				l_Candidates.add(new AnswerCandidate(l_NameLemma, a_Text[i]));
			}
		}
//		for (AnswerCandidate answerCandidate : l_Candidates) {
//			System.out.println("AnswerCandidate=" + answerCandidate.GetAnswer());
//		}
		AnswerCandidate[] l_Result = new AnswerCandidate[l_Candidates.size()];
		return l_Candidates.toArray(l_Result);
	}
	
	/**
	 * Compiles string pattern into a {@link Pattern} and calls {@link #ExtractPattern(Pattern, Passage[])}.
	 * @param a_Pattern
	 * @param a_Text
	 * @return
	 */
	public final static AnswerCandidate[] ExtractPattern(String a_Pattern, Passage[] a_Text) {
	    Pattern l_Pattern = Pattern.compile(a_Pattern);
	    return ExtractPattern(l_Pattern, a_Text);
	}
	
	/**
	 * Extract pattern from passage <code>TEXT</code> field
	 * @param a_Pattern Pattern to extract
	 * @param a_Text Passages to extract pattern from
	 * @return Answer candidates from the passages matching the given pattern
	 */
	public final static AnswerCandidate[] ExtractPattern(Pattern a_Pattern, Passage[] a_Text) {
		LinkedList<AnswerCandidate> l_Candidates = new LinkedList<AnswerCandidate>();
		for (int i = 0; i < a_Text.length; i++) {
			String l_Sentence = a_Text[i].TEXT();
			Matcher l_Matches = a_Pattern.matcher(l_Sentence);
			while (l_Matches.find()) {
				l_Candidates.add(new AnswerCandidate(l_Matches.group(), a_Text[i]));
			}
		}
		AnswerCandidate[] l_Result = new AnswerCandidate[l_Candidates.size()];
		return l_Candidates.toArray(l_Result);
		
	}
	/**
	 * Calls {@link #DefaultGetSubjectOfQuestionWithExpression(Map, String)} with 
	 * POS expression <code>"^[nsexlt]."</code>, matching nouns, verbs, ...,
	 * @param a_Question
	 * @return from {@link #DefaultGetSubjectOfQuestionWithExpression(Map, String)}
	 */
	public final static Entry<String, String> DefaultGetSubjectOfQuestion(Map<String, String> a_Question) {
		return AnswererUtil.DefaultGetSubjectOfQuestionWithExpression(a_Question, "^[nsexlt].*");
	}
	
	/**
	 * Extract words from text that match the given POS expression
	 * @param a_Question Question containing <code>POS</code> and <code>LEMMA</code> fields.
	 * @param a_POSExpr 
	 * @return
	 */
    // TODO: make Passage from question instead of map.
	public final static Entry<String, String> DefaultGetSubjectOfQuestionWithExpression(Map<String, String> a_Question, String a_POSExpr) {
		String l_Field = "TEXT-ANNOTATED-IceLemmald";
		String l_POSText = a_Question.get("POS");
		String l_Lemma = a_Question.get("LEMMA");
		String l_Result = TagParser.FilterOutLemma(l_POSText, l_Lemma, a_POSExpr);
		System.out.println("Lucene Query: " + l_Result);
		return new SimpleEntry<String, String>(l_Field, l_Result);
	}
	
}
