/**
 * 
 */
package is.iclt.icenlp.iceqa.commons;

import java.util.Map;

import sg.edu.nus.wing.qanus.framework.commons.IRegisterableModule;
import sg.edu.nus.wing.qanus.stock.ar.AnswerCandidate;
import sg.edu.nus.wing.qanus.stock.ar.featurescoring.FeatureScorer;

/**
 * @author Olafur Pall Geirsson
 * @version Jul 12, 2013
 * 
 */
public interface IAnswerer extends IRegisterableModule {

	
	/**
	 * @return
	 */
	public String Classname();
	/**
	 * An IAnswerer "subscribes" to types of questions which it then tries to answer
	 * 
	 * @return Example types of questions, can include POS tagging
	 */
	public String[] Subscriptions();

	/**
	 * Extract keywords from question to make a query for the information base
	 * @param a_Question Contains values for keys POS, QC, TEXT, LEMMA
	 * @return String query to information base
	 */
	public String GetQuery(Passage a_Question);
	
	
	/**
	 * The Lucene field to query Lucene
	 * @return
	 */
	public String GetFieldToQueryLucene();
	
	/**
	 * The Lucene field where candidates will be extracted
	 * @return
	 */
	public String GetFieldToExtractCandidates();
	
	public AnswerCandidate[] ExtractCandidates(Passage[] a_BestSentences);
}
