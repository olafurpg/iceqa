package is.iclt.icenlp.iceqa.commons;

public class Answer {
    public String question;
    public String answer;
    public String link;

    public Answer(String question, String answer, String link) {
        super();
        this.question = question;
        this.answer = answer;
        this.link = link;
    }
    /**
     * @return the question
     */
    public String getQuestion() {
        return question;
    }
    /**
     * @param question the question to set
     */
    public void setQuestion(String question) {
        this.question = question;
    }
    /**
     * @return the answer
     */
    public String getAnswer() {
        return answer;
    }
    /**
     * @param answer the answer to set
     */
    public void setAnswer(String answer) {
        this.answer = answer;
    }
    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }
    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }
    
}
