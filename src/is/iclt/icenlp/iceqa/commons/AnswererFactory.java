/**
 * 
 */
package is.iclt.icenlp.iceqa.commons;

/**
 * @author Olafur Pall Geirsson
 * @version Jul 15, 2013
 *
 */
public final class AnswererFactory {

	public static IAnswerer GenerateAnswerer(String a_Classname) {
		Class l_TheClass = null;
		IAnswerer l_Answerer = null;
	    try {
	      l_TheClass = Class.forName(a_Classname);
	      l_Answerer = (IAnswerer)l_TheClass.newInstance();
	    }
	    catch ( ClassNotFoundException ex ){
	      System.err.println( ex + " Interpreter class must be in class path.");
	    }
	    catch( InstantiationException ex ){
	      System.err.println( ex + " Interpreter class must be concrete.");
	    }
	    catch( IllegalAccessException ex ){
	      System.err.println( ex + " Interpreter class must have a no-arg constructor.");
	    }
	    
	    return l_Answerer;
	  }
}
