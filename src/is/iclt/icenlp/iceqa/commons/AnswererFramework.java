/**
 * 
 */
package is.iclt.icenlp.iceqa.commons;

import sg.edu.nus.wing.qanus.stock.ibp.InformationBaseWithLucene;

/**
 * <p>Framework for managing {@link IAnswerer} modules. 
 * 
 * <p>Limited to the QANUS framework using {@link InformationBaseWithLucene}
 * as an information base builder.
 * @author Olafur Pall Geirsson
 * @version Jul 12, 2013
 *
 */
public interface AnswererFramework {
	
	/**
	 * @return Answerer modules to use for Answer Retrieval stage
	 */
	public IAnswerer[] GetAnswerers();
	

	/**
	 * Prepare Answerer modules for Answer Retrieval
	 */
	public boolean LoadAnswerers(InformationBaseWithLucene a_Builder);
	
}
