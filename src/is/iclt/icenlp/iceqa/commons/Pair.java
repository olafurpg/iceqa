/**
 * 
 */
package is.iclt.icenlp.iceqa.commons;

import java.util.Map.Entry;

/**
 * @author ollie
 * @version Jul 31, 2013
 *
 */
public class Pair implements Entry<String, String> {

	private String m_Key;
	private String m_Value;
	
	public Pair() {
		this("", "");
	}
	public Pair(String a_Key, String a_Value) {
		m_Key = a_Key;
		m_Value = a_Value;
	}
	
	@Override
	public String getKey() {
		return m_Key;
	}

	@Override
	public String getValue() {
		return m_Value;
	}

	@Override
	public String setValue(String a_Value) {
		m_Value = a_Value;
		return m_Value;
	}

}
