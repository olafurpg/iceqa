package is.iclt.icenlp.iceqa.test;

import is.iclt.icenlp.iceqa.textprocessing.IcePOS;

import java.util.LinkedList;

import sg.edu.nus.wing.qanus.framework.commons.ITextProcessingModule;

public class IceTextprocessingTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		LinkedList<ITextProcessingModule> tp = new LinkedList<ITextProcessingModule>();
		tp.add(new IcePOS());
		
		LinkedList<String> sentences = new LinkedList<String>();
		sentences.add("Ljóti maðurinn fór yfir gamla húsið");
		sentences.add("Konan er orðin þreytt");
		sentences.add("Jóhann Páll er á forsetalista, honum fannst leiðinlegt í skólanum.");

		for (ITextProcessingModule tpm : tp) {
			System.out.println("Module: " + tpm.GetModuleID());
			String [] s = tpm.ProcessText(sentences.toArray(new String[0]));
			for (String taggedString : s) {
				System.out.println(taggedString);
			}			
		}
	}

}
