/**
 * 
 */
package is.iclt.icenlp.iceqa.test;

import is.iclt.icenlp.iceqa.textprocessing.IceQC;

/**
 * @author Olafur Pall Geirsson
 * @version Jul 16, 2013
 *
 */
public class IceQCTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String[] l_Questions = {
				"Fáni hvaða lands hefur þrjár lárréttar rendur þar sem efsta og neðsta röndin er gul og röndin í miðjunni rauð?",
				"Hvar bjó Hannes Frímansson árið 1770?",
				"Hvar bjó Hannes Frímansson árið 1771?",
				"Hvar dó Paul Erdös?",
				"Hvar er Gnitakór?",
				"Hvar er Vasílíjdómkirkjan?",
				"Hvar er mesti íbúaþéttleiki í heimi?",
				"Hvar er Ólymposfjall?",
				"Hvar finnast Mjónefir?",
				"Hvar finnast mjónefir?",
				"Hvar fæddist Cicero?",
				"Hvar fæddist Hannes Finnsson?",
				"Hvar fæddist Heinrich Rudolf Hertz?",
				"Hvar fæddist Jerome S. Bruner?",
				"Hvar fæddist Paul Erdös?",
				"Hvar nam Auður Ketilsdóttir flatnefs land?",
				"Hvar situr málverkið 'síðasta kvöldmáltíðin' eftir Leonardó da Vinci?",
				"Hvar stigu skipverjar af Mayflower á land?",
				"Hvar var pökkur notaður fyrst í íshokkí?",
				"Hvar voru Ólympíuleikarnir haldnir árið 2000?",
				"Hvað er lengsta fljótið í heiminum?",
				"Hvað er næstlengsta fljót í heiminum?",
				"Hvað er vatnsmesta fljót í heimi?",
				"Hvaða danski málfræðingur fæddist árið 1787?",
				"Hvaða frægi heimspekingur fæddist 28. júní 1712 í Genf?",
				"Hvaða frægi tölvunarfræðingur hóf störf við Stanford-háskóla árið 1968?",
				"Hvaða land rennur Amasónfljótið að meystu leyti til í gegnum?",
				"Hvaða listamaður málaði málverkið 'Mona Lisa'?",
				"Hvaða ár fæddist Anne Holtsmark?",
				"Hvaða ár fæddist Blaise Pascal?",
				"Hvaða ár fæddist Hannes Finnson",
				"Hvaða ár fæddist Hannes Finnsson?",
				"Hvaða ár fæddist leikarinn Christopher Reeve?",
				"Hvaða ár fékk Halldór Kiljan Laxness Nóbelsverðlaunin í bókmenntum?",
				"Hvaða ár fékk Kofi Annan friðarverðlaun Nóbels?",
				"Hvaða ár fékk Wilhelm Röntgen Nóbelsverðlaunin í eðlisfræði?",
				"Hvaða ár komu kanínur fyrst til Íslands?",
				"Hvaða ár sannaði Andrew Wiles síðustu setningu Fermats?",
				"Hvaða ár sannaði Gödel fullkomleikasetninguna?",
				"Hvaða ár setti Haile Gebrselassie frá Eþíópíu heimsmet í maraþonhlaupi karla?",
				"Hvaða ár skrifaði Pascal grein um Pascalþríhyrninginn?",
				"Hvaða ár vann Vala Flosadóttir til verðlauna á Ólympíuleikum?",
				"Hvaða ár var Eurovision haldið í Noregi?",
				"Hvaða ár var Kúbudeilan?",
				"Hvaða ár var stytta Ingólfs Arnarsonar á Arnarhóli afhjúpuð?" ,
				"Hvaða ár var í fyrsta sinn keppt í íshokkí á Ólympíuleikum?",
				"Hvaða ár varð Spútnik-áfallið?",
				"Hvaða ár voru Nóbelsverðlaunin í eðlisfræði veitt í fyrsta skipti?",
				"Hvaða íslendingur hefur fengið Nóbelsverðlaunin?",
				"Hvaðan kemur golf?",
				"Hvaðan var heimspekingurin Þales?",
				"Hvenær dó Napóleon II?",
				"Hvenær dó Paul Erdös?",
				"Hvenær er talið að golf-íþróttin hafi verið fundið upp í Skotlandi?",
				"Hvenær fæddist Donald Knuth?",
				"Hvenær fæddist Nicola Tesla?",
				"Hvenær fæddist Paul Erdös?",
				"Hvenær fæddist Thomas Alva Edison?",
				"Hvenær fór fyrsti háskólakörfuboltaleikurinn fram í Bandaríkjunum?",
				"Hvenær gerði Linus Torvalds fyrstu útgáfuna af Linux stýrikerfinu?",
				"Hvenær hóf Donald Knuth störf við Stanford-háskóla?",
				"Hvenær keyptu Bandaríkin Alaska af Rússlandi?",
				"Hvenær kom íshokkí fyrst til íslands?",
				"Hvenær var HTML staðallinn birtur?",
				"Hvenær var Kanadíska áhugamannadeildin í íshokkí (Canadian Amateur Hockey League) stofnuð?",
				"Hvenær var NHA deildinni breytt í NHL deildina í íshokkí?",
				"Hvenær var heimsmeistaratitill fyrstur veittur í körfubolta karla?",
				"Hvenær var heimsmeistaratitill fyrstur veittur í körfubolta kvenna?",
				"Hvenær var pökkur notaður fyrst í íshokkí?",
				"Hvenær var rússneski sósíaldemókrataflokkurinn stofnaður?",
				"Hvenær var stoðaskipulagið afnumið?",
				"Hvenær varð körfubolti að ólympíuíþrótt?",
				"Hver er afi Hannesar Finssonar?",
				"Hver er frægastur forngrískra vísindamanna?",
				"Hver er höfuðborg Hollands?",
				"Hver fann upp körfubolta?",
				"Hver fékk nóbelsverðlaunin í eðlisfræði árið 1901?",
				"Hver hannaði norska fánann?",
				"Hver nam mestan hluta Borgarfjarðarhéraðs?",
				"Hver samdi leikritið Skýin?",
				"Hver sannaði að 26 væri eina talan sem klemmd er á milli ferningstölu (25) og teningstölu (27)?",
				"Hver sannaði síðustu setningu Fermats?",
				"Hver skilgreindi sykurstuðulinn?",
				"Hver skrifaði Harmonies Economiqes?",
				"Hver skrifaði Heimskringlu?",
				"Hver skrifaði verkið Auðfræðin?",
				"Hver vann til bronsverðlauna í Ólympíuleikunum árið 2000?",
				"Hver var einn helsti hugsuður endurreisnarinnar á Ítalíu?",
				"Hver var forseti Bandaríkjanna í Kúbudeilunni?",
				"Hver var fyrsta húsfreyjan í Reykjavík?",
				"Hver var ríkisstjóri Suður Karólínu árið 1948?",
				"Hvert má rekja upphaf skylminga?",
				"Hvert var fyrsta stýrikerfi Microsoft?",
				"Síðan hvenær hefur afkoma Vatnajökuls verið skráð?",
				"Við hvaða háskóla hóf Donald Knuth störf árið 1968.",
				"Við hvern er sú tilgáta kennd að sérhverja slétta tölu, stærri en 2, megi skrifa sem summu tveggja frumtalna.",
				"hvaða ár gaf Schopenhauer út stutt verk um litaskynjun?",
				"Í hvaða borg fæddist Machiavelli",
				"Í hvaða borg fæddist Vere Gordon Childe?",
				"Í hvaða landi er Notre Dame-dómkirkjan?"
		};
		IceQC l_QC = new IceQC();
		for (String l_Question : l_Questions) {
			System.out.println(l_QC.Classify(l_Question) + ": " + l_Question);
		}

	}

}
