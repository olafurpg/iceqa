package is.iclt.icenlp.iceqa.test;

import is.iclt.icenlp.iceqa.commons.TagParser;

public class AnswererTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String foo = "Rask/nhen lauk/sfg3eþ ritgerðinni/nveþg 1814/ta og/c sendi/sfg3eþ hana/fpveo heim/aa frá/aþ Íslandi/nheþ-s ./. Hún/fpven kom/sfg3eþ þó/aa ekki/aa út/aa fyrr/aam en/c 1818/ta ./. Þá/aa var/sfg3eþ Rask/nkeo-s lagður/sþgken af/aþ stað/nkeþ til/ae Indlands/nhee-s og/c hafði/sfg3eþ komið/ssg auga/nheþ á/ao ýmsa/fokfo vankanta/nkfo ;/; til/ae dæmis/nhee séð/ssg að/c keltnesku/lveþvf málin/nhfng voru/sfg3fþ skyld/lhfnsf íslensku/lkfnvf og/c einnig/aa indversk/lhfnsf og/c írönsk/lhfnsf mál/nhfn ,/, svo/aa sem/aa sanskríts/lheesf og/c persneska/lheþvf ./.";
		String bar = "Rask/rask lauk/ljúka ritgerðinni/ritgerð 1814/1814 og/og sendi/senda hana/hún heim/heim frá/frá Íslandi/ísland ./. Hún/hún kom/koma þó/þó ekki/ekki út/út fyrr/fyrr en/en 1818/1818 ./. Þá/þá var/vera Rask/raskur lagður/leggja af/af stað/staður til/til Indlands/indland og/og hafði/hafa komið/koma auga/auga á/á ýmsa/ýmis vankanta/vankantur ;/; til/til dæmis/dæmi séð/sjá að/að keltnesku/keltneskur málin/mál voru/vera skyld/skyldur íslensku/íslenskur og/og einnig/einnig indversk/indverskur og/og írönsk/írönskur mál/mál ,/, svo/svo sem/sem sanskríts/sanskrítur og/og persneska/persneskur ./. ";
		String kas = TagParser.FilterOutLemma(foo, bar, "^(s|n).*");
		System.out.println(kas);
	}

}
