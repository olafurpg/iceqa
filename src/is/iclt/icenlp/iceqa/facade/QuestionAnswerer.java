package is.iclt.icenlp.iceqa.facade;

import is.iclt.icenlp.core.formald.tags.TagFormat;
import is.iclt.icenlp.core.formald.tags.TaggedText;
import is.iclt.icenlp.core.lemmald.Lemmald;
import is.iclt.icenlp.facade.IceNLP;
import is.iclt.icenlp.formald.tags.Ice3TagFormat;
import is.iclt.icenlp.formald.tags.Ice4TagFormat;
import is.iclt.icenlp.iceqa.ar.featurescoring.FeatureScoringStrategy;
import is.iclt.icenlp.iceqa.commons.Answer;
import is.iclt.icenlp.iceqa.textprocessing.IceLemmald;
import is.iclt.icenlp.iceqa.textprocessing.IcePOS;
import is.iclt.icenlp.iceqa.textprocessing.IceQC;

import java.io.File;

import sg.edu.nus.wing.qanus.framework.commons.DataItem;

/**
 * IceQA API for usage in multi-threaded environment
 * 
 * Access through {@link #getInstance(String)}
 * @author Ólafur Páll Geirsson
 * @version Sep 14, 2013
 *
 */
public class QuestionAnswerer {
    private FeatureScoringStrategy ask;
    private IceLemmald icelemmald;
    private IceQC iceqc;
    private IcePOS icepos;
    private IceNLP nlp;
    private Integer id;
    private Lemmald lemma;
    private TagFormat format,
                        formatLemma;
    private String index; 
    
    private static volatile QuestionAnswerer instance;
    
    /**
     * Get a unique instance of QuestionAnswerer
     * 
     * 
     * @param index Path to Lucene-index to answer questions from, example <code>"./index/Lucene-Index"</code>
     * @return Existing instance from memory if existing, new instance otherwise
     */
    public static QuestionAnswerer getInstance(String index) {
        if (instance == null) {
            synchronized(QuestionAnswerer.class){
                if(instance == null) {
                    instance = new QuestionAnswerer(index);
                }
            }
        }
        return instance;
    }
    
    public QuestionAnswerer(String indexFolder) {
        System.out.println("Constructor");
        index = indexFolder;
        ask = new FeatureScoringStrategy(new File(indexFolder));
        icelemmald = new IceLemmald();
        icepos = new IcePOS();
        iceqc = new IceQC();
        nlp = IceNLP.getInstance();
        format = new Ice3TagFormat();
        formatLemma = new Ice4TagFormat();
        lemma = Lemmald.getInstance();
        id = 0;
    }
    
    public synchronized Answer answer(String question) {
//        System.out.println(System.getProperty("java.class.path"));
        TaggedText tt = nlp.tagAndLemmatizeText(question);
        lemma.lemmatizeTagged(tt);
        String qc = iceqc.Classify(question),
                pos = nlp.tagLines(question).toString(format),
                lemma = tt.toString(formatLemma);
        DataItem q = new DataItem("Question");
        q.AddAttribute("id", id.toString()); id++;
        q.AddAttribute("type", "FACTOID");
        q.AddField("q", question);
        q.AddField("Q-IceQC", qc);
        q.AddField("Q-IcePOS", pos);
        q.AddField("Q-IceLemmald", lemma);
        DataItem answerItem = ask.GetAnswerForQuestion(q, true);
//        System.out.printf("q=%s\nqc=%s\npos=%s\nlemma=%s\n",
//                question,
//                qc,
//                pos,
//                lemma
//                );
        String answerText = "Fann ekkert svar";
        String answerSource = "";
        if (answerItem.GetFieldValues("Answer")[0].GetValue() != null) {
            answerText = answerItem.GetFieldValues("Answer")[0].GetValue()[0];
            if (answerItem.GetFieldValues("Source")[0].GetValue() != null) {
                answerSource = answerItem.GetFieldValues("Source")[0].GetValue()[0];
            }
        }
        return new Answer(question, answerText, answerSource);
    }
    
    public static void main(String[] args) {
        String index = "./index/Lucene-Index";
//        String ans = ask.answer("Hvaða ár sannaði Gödel fullkomleikasetninguna?");
        Answer ans = QuestionAnswerer.getInstance(index).answer("Hver er þekktasti núlifandi tölvunarfræðingurinn?");
//        ans = QuestionAnswerer.getInstance(index).answer("Hvenær fæddist Nicola Tesla?");
        System.out.printf("Answer: %s\nSource: %s\n", ans.answer, ans.link);
    }
}
