package is.iclt.icenlp.formald.tags;

import is.iclt.icenlp.core.formald.tags.TagFormat;
import is.iclt.icenlp.core.formald.tags.TaggedSentence;
import is.iclt.icenlp.core.formald.tags.TaggedText;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Ice3TagFormat extends TagFormat {

    private static final String SENTENCE_BOUNDARY = System.getProperty("line.separator"); 
    private static final String TOKEN_BOUNDARY = " ";
    private String VALUE_BOUNDARY;
    
	public Ice3TagFormat() {
		VALUE_BOUNDARY = "/";
	}
	public Ice3TagFormat(String a_Boundary) {
		VALUE_BOUNDARY = a_Boundary;
	}
	
	@Override
	public Document decode(String data) {
        final String[] sentences = data.split( SENTENCE_BOUNDARY );
        TaggedText taggedText = TaggedText.newInstance();
        
        for( String sentence : sentences ){
            TaggedSentence taggedSentence = taggedText.createSentence();
            final String[] tokens = sentence.split( TOKEN_BOUNDARY );            
            for( int i=0; i<tokens.length; i+=2 ){
                String word = tokens[i].trim();
                // TODO: decode lemma
                String tag = tokens[i+1].trim(); 
                taggedSentence.createToken(word,tag);
            }            
        }
        return taggedText.getDocument();        
    }

	@Override
	public String encode(Document data) {
        StringBuilder output = new StringBuilder();

        Element docRoot = data.getDocumentElement();
        NodeList docSentences = docRoot.getElementsByTagName("sentence");
        for( int i=0; i<docSentences.getLength(); i++ ){
            Element sentence = (Element) docSentences.item(i);
            NodeList docTokens = sentence.getElementsByTagName("token");
            for( int j=0; j<docTokens.getLength(); j++){
                Element docToken = (Element) docTokens.item(j);
                String word = docToken.getAttribute("word");
                String tag = docToken.getAttribute("tag");
                output.append( word + VALUE_BOUNDARY + tag  );
                if( j < docTokens.getLength()-1 ){
                    output.append(TOKEN_BOUNDARY);
                }
            }

            if( i < docSentences.getLength()-1 ){
                output.append( SENTENCE_BOUNDARY );
            }
        }

        return output.toString();
    }

    public static TagFormat newInstance(){
        return new Ice3TagFormat();
    }
    
	@Override
	public String sampleData() {
        return "Ég/fp1en er/sfg1en rauður/lkensf kaktus/nken"+System.getProperty("line.separator")+"og/c ég/fp1en ætla/sfg1en að/cn spila/sng þetta/faheo lag/nheo ./.";
    }

}
