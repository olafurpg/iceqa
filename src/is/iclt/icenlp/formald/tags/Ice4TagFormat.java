package is.iclt.icenlp.formald.tags;

import is.iclt.icenlp.core.formald.tags.TagFormat;
import is.iclt.icenlp.core.formald.tags.TaggedSentence;
import is.iclt.icenlp.core.formald.tags.TaggedText;
import is.iclt.icenlp.core.formald.tags.TaggedToken;

import java.util.List;

import org.w3c.dom.Document;

/**
 * IceQA Lemmald formatter
 * 
 * Word and lemma are separated by '/'
 *  
 * Example: word/lemma word/lemma
 * 
 * @author Olafur Pall Geirsson
 * @version Jul 5, 2013
 *
 */
public class Ice4TagFormat extends TagFormat {

    private static final String SENTENCE_BOUNDARY = System.getProperty("line.separator"); 
    private static final String TOKEN_BOUNDARY = " ";
    private String VALUE_BOUNDARY;
    
	public Ice4TagFormat() { 
		VALUE_BOUNDARY = "/";
	}
	public Ice4TagFormat(String a_Boundary) {
		VALUE_BOUNDARY = a_Boundary;
	}
	
	@Override
	public Document decode(String data) {
        final String[] sentences = data.split( SENTENCE_BOUNDARY );
        TaggedText taggedText = TaggedText.newInstance();
        
        for( String sentence : sentences ){
            TaggedSentence taggedSentence = taggedText.createSentence();
            final String[] tokens = sentence.split( TOKEN_BOUNDARY );            
            for( int i=0; i<tokens.length; i+=2 ){
                String word = tokens[i].trim();
                String lemma = tokens[i+1].trim(); 
                taggedSentence.createToken(word,"", lemma); // not tag available
            }            
        }
        return taggedText.getDocument();        
    }

	@Override
    public String encode(Document data) {
        StringBuilder output = new StringBuilder();
        TaggedText taggedText = TaggedText.newInstance(data);
        List<TaggedSentence> sentences = taggedText.getSentences();

        for( TaggedSentence sentence : sentences ){
            List<TaggedToken> tokens = sentence.getTokens();
            for( TaggedToken token : tokens ){
                output.append( token.getWord() );
                if( token.hasLemma() ){
                    output.append(VALUE_BOUNDARY + token.getLemma());
                } else {
                	output.append(VALUE_BOUNDARY + "LEMMA_MISSING");
                }
                output.append(TOKEN_BOUNDARY);
            }
//            output.append( System.getProperty("line.separator")  );
        }


        return output.toString();
    }

    public static TagFormat newInstance(){
        return new Ice4TagFormat();
    }
    
	@Override
	public String sampleData() {
        return "Ég/fp1en er/sfg1en rauður/lkensf kaktus/nken"+System.getProperty("line.separator")+"og/c ég/fp1en ætla/sfg1en að/cn spila/sng þetta/faheo lag/nheo ./.";
    }

}
