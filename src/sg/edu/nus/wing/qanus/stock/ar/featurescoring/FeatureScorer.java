package sg.edu.nus.wing.qanus.stock.ar.featurescoring;


import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import sg.edu.nus.wing.qanus.stock.ar.DocumentScore;


/**
 * Responsible for tabulating the scores of candidate passages.
 * 
 * @author NG, Jun Ping -- junping@comp.nus.edu.sg
 * @version v04Jan2010
 */
public class FeatureScorer {

	// Used to hold all the added documents
	private ArrayList<Pair> m_DocumentStore;

	
	/**
	 * A POJO helper class to keep track of document index in the DataItem
	 * @author Ólafur Páll Geirsson
	 * @version Aug 9, 2013
	 *
	 */
	public class Pair {
		String m_Document;
		int m_Index;
		public Pair(String m_Document, int m_Index) {
			super();
			this.m_Document = m_Document;
			this.m_Index = m_Index;
		}
		/**
		 * @return the m_Document
		 */
		public String getDocument() {
			return m_Document;
		}
		/**
		 * @return the m_Index
		 */
		public int getIndex() {
			return m_Index;
		}
		
	}

	/**
	 * Constructor
	 */
	public FeatureScorer() {
		m_DocumentStore = new ArrayList<Pair>();
	} // end constructor


	/**
	 * Adds a document to the class for subsequent consideration.
	 * @param a_DocumentString [in] the actual string that make up the document.
	 */
	public void AddDocument(String a_DocumentString, int a_Index) {
		m_DocumentStore.add(new Pair(a_DocumentString, a_Index));
	} // end AddDocument()
	
	/**
	 * Adds a document to the class for subsequent consideration.
	 * @param a_DocumentString [in] the actual string that make up the document.
	 */
	public void AddDocument(String a_DocumentString) {
		m_DocumentStore.add(new Pair(a_DocumentString, 0));
	} // end AddDocument()


	/**
	 * Retrieve the top N documents based on the scores assigned to the documents.
	 *
	 * @param a_Query [in] search query used to score the documents against
	 * @param a_NumTopDocs [in] the number of documents to retrieve
	 * @return array of strings of the top N documents
	 */
	public String[] RetrieveTopDocuments(String a_Query, int a_NumTopDocs) {
		Pair[] l_DocumentPairs = RetrieveTopDocumentsPairs(a_Query, a_NumTopDocs);
		return GetDocuments(l_DocumentPairs);
	}
	
	/**
	 * Extract document strings from pairs
	 * @param a_Documents
	 * @return Array of document strings
	 */
	public static String[] GetDocuments(Pair[] a_Documents) {
		String[] l_TopDocuments = new String[a_Documents.length];
		for (int i = 0; i < a_Documents.length; i++) {
			l_TopDocuments[i] = a_Documents[i].getDocument(); 
		}
		return l_TopDocuments;
	}
	
	
	/**
	 * @param a_Query
	 * @param a_NumTopDocs
	 * @return
	 */
	public Pair[] RetrieveTopDocumentsPairs(String a_Query, int a_NumTopDocs) {


		// We use a priority queue to quickly retrive top scoring documents
		PriorityQueue<DocumentScore> l_TopDocuments = new PriorityQueue<DocumentScore>();	
//		Logger.getLogger("QANUS").setLevel(Level.ALL);
		
		Logger.getLogger("QANUS").log(Level.FINER, "Retrieving [" + a_NumTopDocs + "] documents for query [" + a_Query + "]");


		// Init the features to use - new features can be added here
		FeatureSearchTermFrequency l_Feature_Frequency = new FeatureSearchTermFrequency();
		FeatureSearchTermSpan l_Feature_Proximity = new FeatureSearchTermSpan();
		FeatureSearchTermCoverage l_Feature_Coverage = new FeatureSearchTermCoverage();
		

		// Calculate score for each document
		int l_DocIndex = 0;
		for (Pair l_DocumentPair : m_DocumentStore) {
			String l_Document = l_DocumentPair.getDocument();
			double l_DocScore = 0;

			String[] l_QueryArray = { a_Query };

			// Invoke the various features
			double l_FreqScore = l_Feature_Frequency.GetScore(l_QueryArray, l_Document);
			l_DocScore += (0.05)*l_FreqScore;

			double l_ProxScore = l_Feature_Proximity.GetScore(l_QueryArray, l_Document);
			l_DocScore += (0.05)*l_ProxScore;

			double l_CoverageScore = l_Feature_Coverage.GetScore(l_QueryArray, l_Document);
			l_DocScore += (0.9)*l_CoverageScore;

			Logger.getLogger("QANUS").log(Level.FINER, "F:" + l_FreqScore + "-P:" + l_ProxScore + "-C:" + l_CoverageScore + "-[" + l_Document + "]");
			
			l_TopDocuments.add(new DocumentScore(l_DocIndex, l_DocScore, l_Document, l_DocumentPair.getIndex()));
			l_DocIndex++;

		} // end String l_Document..
			
		

		// Create a result array to return the top X documents in		
		Pair[] l_ResultArray = new Pair[(a_NumTopDocs<l_TopDocuments.size())?a_NumTopDocs:l_TopDocuments.size()];
		for (int i = 0; i < l_ResultArray.length; ++i) {
			DocumentScore l_TopDoc = l_TopDocuments.remove();
			l_ResultArray[i] = new Pair(l_TopDoc.GetDocText(), l_TopDoc.getDocIndex());
		} // end for i

		return l_ResultArray;

		
	} // end RetrieveTopDocuments()
	

} // end class FeatureScorer
