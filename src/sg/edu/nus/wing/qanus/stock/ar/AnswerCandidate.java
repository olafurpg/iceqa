
package sg.edu.nus.wing.qanus.stock.ar;

import is.iclt.icenlp.iceqa.commons.Passage;

/**
 * Used to store an answer candidate, alongside the source passage from which it
 * is extracted.
 *
 * @author NG, Jun Ping -- junping@comp.nus.edu.sg
 * @version 04Jan2010
 */
public class AnswerCandidate implements Comparable<AnswerCandidate> {

	private String m_Answer;
	private Passage m_Source;
	private String m_Source_Orig;
	private int m_IndexOfSource;
	
	

	/**
	 * Answer and Source passage
	 * @param m_Answer
	 * @param m_Source
	 * @author Ólafur Páll Geirsson
	 */
	public AnswerCandidate(String m_Answer, Passage m_Source) {
		super();
		this.m_Answer = m_Answer;
		this.m_Source = m_Source;
	}


	/**
	 * 
	 * @param m_Answer 
	 * @param m_Source_Orig
	 * @param m_IndexOfSource Index of the source string in DatItem
	 */
	public AnswerCandidate(String m_Answer, String m_Source,
			int m_IndexOfSource) {
		super();
		this.m_Answer = m_Answer;
		this.m_Source = null;
		this.m_Source_Orig = m_Source;
		this.m_IndexOfSource = m_IndexOfSource;
	}


	/**
	 * Constructor
	 * @param a_Answer [in] answer candidate string
	 * @param a_Source [in] original source of candidate
	 * @param a_SourcewTags [in] source of candidate annotated with POS
	 */
	public AnswerCandidate(String a_Answer, String a_Source) {
		m_Answer = a_Answer;
		m_Source_Orig = a_Source;		
	}

	public String GetAnswer() {
		return m_Answer;
	}

	public String GetOrigSource() {
		return m_Source_Orig;
	}


	/**
	 * @return the m_IndexOfSource
	 */
	public int getIndexOfSource() {
		return m_IndexOfSource;
	}
	
	

	/**
	 * @return the m_Source
	 */
	public Passage getSource() {
		return m_Source;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		// AnswerCandidate is unique on the answer itself, not source origin or index
		return m_Answer.hashCode();
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnswerCandidate other = (AnswerCandidate) obj;
		if (m_Answer == null) {
			if (other.m_Answer != null)
				return false;
		} else if (!m_Answer.equals(other.m_Answer))
			return false;
		if (m_IndexOfSource != other.m_IndexOfSource)
			return false;
		if (m_Source_Orig == null) {
			if (other.m_Source_Orig != null)
				return false;
		} else if (!m_Source_Orig.equals(other.m_Source_Orig))
			return false;
		return true;
	}


	@Override
	public int compareTo(AnswerCandidate a_OtherCandidate) {
		return m_Answer.compareTo(a_OtherCandidate.GetAnswer());
	}
	
	
	


} // end class AnswerCandidate
