package sg.edu.nus.wing.qanus.stock.ibp;


import is.iclt.icenlp.iceqa.commons.IAnswerer;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.LockObtainFailedException;

import sg.edu.nus.wing.qanus.framework.commons.DataItem;
import sg.edu.nus.wing.qanus.framework.commons.IInformationBaseBuilder;




/**
 * Implements a Lucene based knowledge base.
 *
 * @author NG, Jun Ping -- junping@comp.nus.edu.sg
 * @version v18Jan2010
 */
public class InformationBaseWithLucene implements IInformationBaseBuilder {


    private String m_LuceneIndexFileName; 
    // Lucene components
    private IndexWriter m_LuceneIW = null;
    private int m_DocCount;

    /**
     * Constructor.
     * @param a_TargetFile [in] Folder where output will be stored
     */
    public InformationBaseWithLucene(File a_TargetFile) {
        this(a_TargetFile, true);
    }
    
    public InformationBaseWithLucene(File a_TargetFile, boolean a_Create) {
        m_DocCount = 0;
        // Build the index writer by Lucene
        try {
            m_LuceneIndexFileName = a_TargetFile + File.separator + "Lucene-Index";
            m_LuceneIW = new IndexWriter(
                    m_LuceneIndexFileName, new StandardAnalyzer(), a_Create,
                    IndexWriter.MaxFieldLength.UNLIMITED);
        } catch (CorruptIndexException e) {
            m_LuceneIW = null;
            Logger.getLogger("QANUS").logp(Level.SEVERE, InformationBaseWithLucene.class.getName(), "Go", "Problem initializing Lucene", e);
        } catch (LockObtainFailedException e) {
            m_LuceneIW = null;
            Logger.getLogger("QANUS").logp(Level.SEVERE, InformationBaseWithLucene.class.getName(), "Go", "Problem initializing Lucene", e);
        } catch (IOException e) {
            m_LuceneIW = null;
            Logger.getLogger("QANUS").logp(Level.SEVERE, InformationBaseWithLucene.class.getName(), "Go", "Problem initializing Lucene", e);
        }


    } // end constructor
    

    /**
     * Close Lucene index using {@link IndexWriter#close()}.
     * @author Ólafur Páll Geirsson 
     * @return True on success, false otherwise
     */
    public boolean CloseWriter() {
        try {
            m_LuceneIW.close();
            return true;
        } catch (CorruptIndexException e) {
            m_LuceneIW = null;
            Logger.getLogger("QANUS").logp(Level.SEVERE, InformationBaseWithLucene.class.getName(), "Go", "Lucene index is corrupt", e);
        } catch (IOException e) {
            m_LuceneIW = null;
            Logger.getLogger("QANUS").logp(Level.SEVERE, InformationBaseWithLucene.class.getName(), "Go", "Problem closing Lucene", e);
        }
        return false;
    }


    /**
     * Given a data item for the <code>&lt;TEXT&gt;</code> XML instance, extract the different <code>&lt;P&gt;</code> sentences within it
     * into a string array.
     * 
     * 
     * @param a_FieldValues [in] list of <code>&lt;TEXT&gt;</code> XML instance data items.
     * @return array of strings of each <code>&lt;P&gt;</code> sentence within the <code>&lt;TEXT&gt;</code> tags.
     */
    private String[] ExtractSentencesIntoArray(DataItem[] a_FieldValues) {

        LinkedList<String> l_SentencesList = new LinkedList<String>();

        // Each data item is a TEXT XML instance.
        // Normally there should only be one, but we'd loop through anyway, just in case
        for (DataItem l_Item : a_FieldValues) {

            DataItem[] l_SentenceValues =  l_Item.GetFieldValues("P");
            if (l_SentenceValues == null || l_SentenceValues.length == 0) continue;  // Nothing to do

            for (DataItem l_SentenceValue : l_SentenceValues) {

                String[] l_SentenceArray = l_SentenceValue.GetValue();
                if (l_SentenceArray == null || l_SentenceArray.length == 0) continue;

                for (String l_Sentence : l_SentenceArray) {
                    l_SentencesList.add(l_Sentence);
                } // end for l_Sentence...

            } // end for l_SentenceValue...

        } // end for l_Item...


        if (l_SentencesList.size() == 0) 
            return new String[0];
        else
            return l_SentencesList.toArray(new String[0]);

    } // end ExtractSentencesIntoArray()

    
    /**
     * Check if Lucene has been initialized
     * @return True if Lucene is uninitialized, false otherwise 
     */
    private boolean LuceneNotInitialized() {
        // Check that Lucene is properly initialized
        if (m_LuceneIW == null) {
            Logger.getLogger("QANUS").logp(Level.SEVERE, InformationBaseWithLucene.class.getName(), "Notify", "Lucene not initialized.");
            return true;
        }
        return false;
    }

    /**
     * Add article to Lucene index. 
     * @param a_Item Article to add to index, must at least contain field <code>id</code>. 
     * Optionally it can contain the fields <code>type, HEADLINE, DATELINE</code>. 
     * and also add any field starting with <code>TEXT</code>.
     * 
     * <p>Multiple text fields are allowed, for example <code>TEXT, TEXT-NER</code> and <code>TEXT-POS</code>. 
     * @return
     */
    public boolean AddArticleToInfoBase(DataItem a_Item) {
        if (LuceneNotInitialized()) {
            return false;
        }

//         Extract information from the data item
        String l_Info_ID = a_Item.GetAttribute("id");
        String l_Info_Type = a_Item.GetAttribute("type");

        DataItem[] l_HeadlineValues = a_Item.GetFieldValues("HEADLINE");
        DataItem[] l_DatelineValues = a_Item.GetFieldValues("DATELINE");

//        Verify retrieve information for validity - esp critical values like DocID
        if (l_Info_ID == null) {
            Logger.getLogger("QANUS").logp(
                    Level.WARNING, 
                    InformationBaseWithLucene.class.getName(),
                    "Notify",
                    "Incomplete information about data item."
                    );
            return false;
        }

        String l_Info_Headline = l_HeadlineValues == null?null:l_HeadlineValues[0].GetValue()[0];
        String l_Info_Dateline = l_DatelineValues == null?null:l_DatelineValues[0].GetValue()[0];

//        Start building document
        Document l_LuceneDoc = new Document();
        l_LuceneDoc.add(new Field("DocID", l_Info_ID, Field.Store.YES, Field.Index.NOT_ANALYZED));
        if (l_Info_Type != null) l_LuceneDoc.add(new Field("Type", l_Info_Type, Field.Store.YES, Field.Index.NO));
        if (l_Info_Headline != null) l_LuceneDoc.add(new Field("Headline", l_Info_Headline, Field.Store.YES, Field.Index.ANALYZED));
        if (l_Info_Dateline != null) l_LuceneDoc.add(new Field("Dateline", l_Info_Dateline, Field.Store.YES, Field.Index.ANALYZED));
    	
//         Add TEXT fields
        String[] l_AllFields = a_Item.GetAllFieldNames();
        for (String l_Field : l_AllFields) {
            if (l_Field.startsWith("TEXT")) { // Annotated values
                DataItem[] l_TextValues = a_Item.GetFieldValues(l_Field);
                String[] l_Sentences = ExtractSentencesIntoArray(l_TextValues);
                if (l_Sentences == null || l_Sentences.length == 0) {
                    Logger.getLogger("QANUS").logp(Level.WARNING, InformationBaseWithLucene.class.getName(), "AddArticleToInfoBase", "Data item has no text.");
                    return false;
            	}
                for (String l_Sentence : l_Sentences) {
//                    l_Article.AddField(l_Field, l_Sentence);
                    l_LuceneDoc.add(new Field(l_Field, l_Sentence, Field.Store.YES, Field.Index.ANALYZED));
            	}
        	}
        }

        return AddDocToInfoBase(l_LuceneDoc);
    }

    
    /**
     * Delete all {@link IAnswerer} documents in Lucene Index
     * @return True on success, false otherwise
     */
    public boolean RemoveAllAnswerersFromInfoBase() {
        try {
            m_LuceneIW.deleteDocuments(new Term("ANSWERER", "IceQA"));
            return true;
        } catch (CorruptIndexException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    	
    }
    
    /**
     * Construct a Document with fields 
     * <Code>CLASSNAME, ANSWERER</code>
     * and <code>SUBSCRIPTION</code> and
     * add to Lucene Index.
     * @param a_Item
     * @return True on success, false otherwise
     */
    public boolean AddAnswererToInfoBase(DataItem a_Item) {
    	
        if (LuceneNotInitialized()) {
            return false;
        }
    	
//        1. Extract class name
        DataItem[] l_Classname = a_Item.GetFieldValues("CLASSNAME");
    	
//         Verify class name exists
        if (l_Classname == null) {
            Logger.getLogger("QANUS").logp(
                    Level.WARNING, 
                    InformationBaseWithLucene.class.getName(), 
                    "AddAnswererToInfoBase", 
                    "Missing classname"
                    );
            return false;
        }

        String l_Info_Classname = l_Classname[0].GetValue()[0];
    	
//         2. Build up the Lucene document
        Document l_LuceneDoc = new Document();

//        Class name		
        l_LuceneDoc.add(new Field("CLASSNAME", l_Info_Classname, Field.Store.YES, Field.Index.NOT_ANALYZED));
        l_LuceneDoc.add(new Field("ANSWERER", "IceQA", Field.Store.YES, Field.Index.NOT_ANALYZED));
    	
//        Subscription text
        for (DataItem l_SubscriptionDataItem : a_Item.GetFieldValues("SUBSCRIPTION")) { 
            String[] l_Subscriptions = l_SubscriptionDataItem.GetValue();
            for (String l_Subscription : l_Subscriptions) {
                l_LuceneDoc.add(new Field("SUBSCRIPTION", l_Subscription, Field.Store.YES, Field.Index.ANALYZED));
        	}
        }
    	
        return AddDocToInfoBase(l_LuceneDoc);
    }
    
    
    @SuppressWarnings("deprecation")
    public boolean AddDocToInfoBase(Document a_LuceneDoc) {
        Date l_Now = new Date();
        System.out.println("Doc " + m_DocCount++ + ": " + a_LuceneDoc.get("Headline") + " (" + l_Now.getHours() + ":" + l_Now.getMinutes() + ") ");
    	
        // 1. Add it to Lucene
        try {
            m_LuceneIW.addDocument(a_LuceneDoc);
        } catch (CorruptIndexException e1) {
            Logger.getLogger("QANUS").logp(Level.WARNING, InformationBaseWithLucene.class.getName(), "Notify", "Unable to add to Lucene", e1);
            return false;
        } catch (IOException e1) {
            Logger.getLogger("QANUS").logp(Level.WARNING, InformationBaseWithLucene.class.getName(), "Notify", "Unable to add to Lucene", e1);
            return false;
        }
        // 2. Commit the add
        try {
            m_LuceneIW.commit();
        } catch (CorruptIndexException e) {
            Logger.getLogger("QANUS").logp(Level.WARNING, InformationBaseWithLucene.class.getName(), "Notify", "Unable to commit", e);
            return false;
        } catch (IOException e) {
            Logger.getLogger("QANUS").logp(Level.WARNING, InformationBaseWithLucene.class.getName(), "Notify", "Unable to commit", e);
            return false;
        }



        return true;

    } // end AddToInfoBase()


    @Override
    public boolean AddToInfoBase(DataItem a_Item) {
        return AddArticleToInfoBase(a_Item);
    }

} // end class InformationBaseWithLucene

