# IceQA

### Text processing

##### IceNLPProcessor
An abstract superclass for processing text in QANUS with IceNLP.

### Answer retrieval    

Answer Retrieval
-----------------
The brain of the system

#### Controller
`IXMLParser`: `TREC2007AnnotatedQuestionXMLParser`     
`IRegisterableModule`: `FeatureScoringStrategy`

#### AnswerRetriever
Unlike the other stages, the method `Notify(DataItem a_item):void` makes use of Java synchronisation for concurrent execution.


#### FeatureScore
Responsible for tabulating the scores of candidate passages.

`AddDocument(String docString):void`      
`RetrieveTopDocuments(String query, int n):String[]`   

#### FeatureScoringStrategy
2000 lines of code.

> Serves as a (somewhat bad) example of how new AR strategies could be added to QANUS.

Contains hardcoded patterns for retrieving an answer for an english corpus. Main function

`GetAnswerForQuestion(DataItem a_QuestionItem, boolean a_Analysis):DataItem` 

is over 1200 lines. Uses the Li and Roth classification, over 2200 question types.

###### Thoughts

* Queries twice, first to the lucene index to retrive a large number of documents and then queries a `FeatureScorer` for fewer answers.

#### FreebaseQuerier
Used to confirm that 

##### Thoughts

Questions
--------------

* What strategy should we use to retrieve the correct answer?
    * QANUS: Comes with a featurescoring framework. See image.
    * My proposal: `IAnswerer` framework. See image.
* IceNLP.newInstance() vs. IceNLP.getInstance()?
* How does IceNER work exactly?
* Should we consider getting access to Gettu Betur questions?
    * It might be a great source of factoid questions
* What is the IFD corpus?
* Which book in bookshelf on Icelandic grammar?
* Are there any available Apache Lucene customizations for Icelandic?
    * Analyser
    * Query parser
* Could I get my name on the list of people? [nlp.cs.ru.is](http://nlp.cs.ru.is/)