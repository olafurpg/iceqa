QANUS
===============
`QANUS` is built with extensibility in mind. An online demo is supposed to be available at [wing.comp.nus.edu.sg/~junping/qanus/online/main.php](http://wing.comp.nus.edu.sg/~junping/qanus/online/main.php) but it is unfortunately offline at the moment.    

### General outline (Commons)

##### BasicController
Entry point for a typical control class in QANUS. 
Used to kick start components like the information-base builder or query processor. 
Provides common functionality for entry point modules, in particular *command line argument processing*.

##### DataItem
Houses a unit of data item parsed by the XML handler.
Typically in a corpus, we can identify a smallest logical unit of information. 
For example in the AQUAINT2 corpus, this is a `<DOC>` element. 

##### IStageEngineController
Configures XML handlers and text-processing modules.     
`ITextProcessingModule` Process text, this can be any NLP tools such as Stanford's POSTagger, ICETagger etc.       
`IXMLParser` Read XML file, this can be an AQUAINT Corpus, TREC Questions, Icelandic Web of Science answers etc.

##### IXMLDataReceipient
Method `Notify(Dataitem a_Item):void` Accepts data items from a parsed XML file (`IXMLParser`), processes it and stores the results into a target file.

##### StageEngine
A basic processing unit for each pipeline stage.
Typically takes in some input, invokes registered processing modules on these input,
and stores the output in another file.

Usually paired with a FrameworkController in a stage which will invoke this class.


Information base processing
----------------------------
Prepares the knowledge base

The framework itself does not make any restrictions on the data input format. `QA-REF` has however been implemented to read the `AQUAINT-2` corpus, xml structured data. 

### AQUAINT Corpus
Information available [here](http://www.ldc.upenn.edu/Catalog/docs/LDC2002T31/)

##### General
This corpus consists of newswire text data in English, drawn from three sources: the Xinhua News Service (People's Republic of China), the New York Times News Service, and the Associated Press Worldstream News Service. It was prepared by the LDC for the AQUAINT Project, and will be used in official benchmark evaluations conducted by National Institute of Standards and Technology (NIST).

##### Organization and Format of the Data

The text data are separated into directories by source (apw, nyt, xie); within each source, data files are subdivided by year, and within each year, there is one file per date of collection. Each file is named to reflect the source and date, and contains a stream of SGML-tagged text data presenting the series of news stories reported on the given date as a concatenation of DOC elements (i.e. blocks of text bounded by <DOC> and </DOC> tags).

A single DTD file is provided [aquaint.dtd](http://www.ldc.upenn.edu/Catalog/docs/LDC2002T31/aquaint.dtd) which serves to define the markup structure for any standard SGML parser. All data files in this corpus have been validated using this DTD.

A piece of article is contained within a single `DOC` element which breaks down as follows.

apw

    (DOC (DOCNO) (DOCTYPE*) (DATE_TIME*) (HEADER) (BODY (SLUG) (HEADLINE*) (TEXT SUBHEAD*) ) ) (TRAILER) )

nyt/1998:

    (DOC (DOCNO) (DOCTYPE) (DATE_TIME) (HEADER) (BODY (SLUG*) (HEADLINE*) (TEXT (ANNOTATION*) ) ) (TRAILER) )

xie/* (all years):
    
    (DOC (DOCNO) (DATE_TIME) (BODY (HEADLINE) (TEXT) ) )

##### Example file
    <?xml version="1.0" encoding="UTF-8"?>
    
    <DOCSTREAM>
    
    <DOC id="AUTOWEEK_909209996" type="story" >
    
    <HEADLINE>
    Renault F1 awaits fate on Monday, team's future unclear
    </HEADLINE>
    
    <DATELINE>
    MIAOWANZHEN, China, Dec. 1
    </DATELINE>
    
    <TEXT>
    <P>
    Formula One's Renault crash saga should finally reach a conclusion in Paris on Monday when the FIA World Motor Sport Council (WMSC) conducts its hearing into Nelson Piquet Jr.'s accident in last year's Singapore Grand Prix.
    </P>
    …
    <P>
    Renault is awaiting its sentence before making a definitive decision about next season.     Having already lost title sponsor ING, the team lacks outside financial support for 2010.     Company boss Carlos Ghosn could decide to withdraw the team from F1 and attempt to sell it, just as Honda and BMW did this year.
    </P>
    </TEXT>
    </DOC>
    </DOCSTREAM>

### Vísindavefur Háskóla Íslands (e. Icelandic Web of Science)
Information about the Icelandic Web of Science available [here](http://www.why.is/article.php?id=65)

##### Organization and format of the data
The Icelandic Web of Science has generously provided us with all 9965 answers from their database. The text data is a single `xml` formated rss feed file. 

The General layout of the received file is as follows.

    (rss (channel (title) (link) (description) (lastBuildDate) (item*)))
    
A single question is contained within the `item` element which further break down as follows.

    (item (title) (pubDate) (description ![CDATA[ANSWER_TEXT]]) (categories (category*)))

##### Example file

    
    <?xml version="1.0" encoding="UTF-8"?>
    <rss version="0.92">
     <channel>
      <title>Vísindavefurinn</title>
      <link>http://www.visindavefur.is</link>
      <description>Á Vísindavefnum svara fræðimenn spurningum almennings á öllum sviðum vísinda og fræða.</description>
      <lastBuildDate>Mon, 17 Jun 2013 00:04:52 GMT</lastBuildDate>
      <item>
       <title>Af hverju var Alþingi stofnað?</title>
       <link>http://www.visindavefur.is/?id=59883</link>
       <pubDate>Fri, 14 Jun 2013 12:30:41 GMT</pubDate>
       <description><![CDATA[ANSWER_TEXT]]></description>
       <categories>
        <category>
         <name>Hugvísindi</name>
         <id>2</id>
        </category>
        <category>
         <name>Is</name>
         <id>138</id>
        </category>
        <category>
         <name>Sagnfræði: Íslandssaga</name>
         <id>20</id>
        </category>
       </categories>
      </item>
     </channel>
    </rss>

The main content of an item element, the answer, is found within the `ANSWER_TEXT` placeholder inside the `description` element. 
Answers are html formatted strings inside CDATA sections in order to escape xml parsing.

       <description><![CDATA[Það hefur tíðkast frá ómunatíð víðs vegar um heiminn að menn komi saman á þing til að ráða ráðum sínum, setja lög og dæma í málum manna. Til er sú skoðun að slíkt almannavald sé eldra og upphaflegra en vald fárra og tiginna stjórnenda eins og konunga. <a href="?id=51981" title="Hvernig var kosningakerfi Grikkja til forna?" alt="Hvernig var kosningakerfi Grikkja til forna?" target="_self">Aþeningar</a> hinir fornu, sem löngum hefur verið litið til sem fyrirmyndar um stjórnvisku, völdu menn til þingsetu með hlutkesti – en raunar aðeins úr röðum frjálsra og karlkyns borgara. Annars staðar sóttu allir vopnfærir karlmenn þing, og er talið að orðið al-þing sér runnið frá slíku þinghaldi. <br>
    <br>
    Rómverskur rithöfundur, <a href="?id=60400" title="Hvar var Tacitus og hvað gerði hann merkilegt?" alt="Hvar var Tacitus og hvað gerði hann merkilegt?" target="_self">Tacitus</a> að nafni, 

    …

    Því má bæta við að þessi siður, að samþykkja með því að slá saman vopnum eða vopnum og skjöldum, mun vera fyrirrennari þess að samþykkja með lófataki.<br>
    <br>
    <table class="img_text img_text_center img_text_noborder" style="float: center" width="180"><tbody><tr><td class=" img_text_noborder" style="float: center" valign="top"><a href="../myndir/gulathing_stor_040613.jpg" class="img_large"><img src="../myndir/gulathing_litil_040613.jpg" align="top" border="0" ></a><br><font size="1"><em></em><center><em>Svona gæti hafa verið umhorfs á Gulaþingi í Noregi þangað sem Íslendingar sóttu líklega fyrirmynd sína að þingi.</em></center></font></td></tr></tbody></table><br>
    
    … 
    
    <br>
    Heimildir og mynd:<br>
    Íslenzk fornrit. Íslendingabók. Landnámabók. Jakob Benediktsson gaf út. Reykjavík, Fornritafélag, 1968.<br>
    Jakob Benediktsson: „Landnám og upphaf allsherjarríkis.“ Saga Íslands I (Reykjavík, Bókmenntafélag, 1968), 153–96.<br>
    Tacitus: Germanía. Þýtt hefir Páll Sveinsson. Reykjavík, Þjóðvinafélag, 1928.<br>
    Ullmann, Walter: Medieval Political Thought. Harmondsworth, Penguin, 1979 (first published 1965).<br>
    Mynd: Gulatinget - Sogn og Fjordane - Fylkesleksikon - NRK. (Sótt 4. 6. 2013).<br>
    <br>
    Í heild hljóðaði spurningin svona:Af hverju var Alþingi stofnað? Hvers konar vandamál voru til staðar áður en það var stofnað?<br>
    ]]></description>
    
#### Controller
`IXMLParser`: `AQUAINT2XMLHandler`     
`IRegisterableModule`: `StanfordPOSTagger`, `StanfordNER`


Textprocessing
-----------------

All textprocessing classes implement the interface `ITextProcessingModule` from the package `sg.edu.nus.wing.qanus.framework.commons`. The interface contains two methods

* `ProcessText` accepts as input an array of strings and returns another array of strings
* `GetModuleID` accepts no input and returns a string including the name of the module

The general strategy I'm going for will be to compare the input/output of the textprocessing modules used in `QA-REF` with `IceNLP`.

`QA-REF` makes extensive use of the Stanford CoreNLP tools, an available demo for all of these tools is available [nlp.stanford.edu:8080/corenlp/](http://nlp.stanford.edu:8080/corenlp/). 


#### Thesaurus.java

`QA-REF` uses [bighugelabs's](http://words.bighugelabs.com/api.php) thesaurus api. Executing the following command

    curl http://words.bighugelabs.com/api/2/b904ec59508fb2adec6cf3ba113005ce/brilliant/

will result in an output of

    adjective|syn|superb
    adjective|syn|brainy
    adjective|syn|smart as a whip
    ...
    adjective|sim|reverberant
    adjective|sim|ringing
    adjective|sim|superior

 The output can also be returned in xml or json format by appending `/json` or 
`/xml` to the URL.

###### Thoughts

* Something comparable to [Snara](http://snara.is/bls/um/_samh.aspx)'s online Thesaurus?
    * Could be possible to write a wrapper
* **Query reformulation** is important for QA systems based on a small corpus according to Speech and Language processing p. 814

#### PorterStemmer.java
An implementation of the Porter stemmer algorithm to transform a word into its root form.

##### Thoughts

* IceNLP's `Lemmald` seems 
* What about [bin.arnastofnun.is](http://bin.arnastofnun.is)?


#### StanfordNER.java
Stanford named entity recogniser, demo available online at [nlp.stanford.edu](http://nlp.stanford.edu:8080/ner/). In `/Users/ollie/src/stanford-ner-2009-01-16`. Running command

    java -classpath stanford-ner-2009-01-16.jar -mx600m edu.stanford.nlp.ie.crf.CRFClassifier -loadClassifier classifiers/ner-eng-ie.crf-3-all2008-distsim.ser.gz -textFile sample.txt

results in output

    The/O fate/O of/O Lehman/ORGANIZATION Brothers/ORGANIZATION ,/O the/O beleaguered/O investment/O bank/O ,/O hung/O in/O the/O balance/O on/O Sunday/O as/O Federal/ORGANIZATION Reserve/ORGANIZATION officials/O and/O the/O leaders/O of/O major/O financial/O institutions/O continued/O to/O gather/O in/O emergency/O meetings/O trying/O to/O complete/O a/O plan/O to/O rescue/O the/O stricken/O bank/O ./O Several/O possible/O plans/O emerged/O from/O the/O talks/O ,/O held/O at/O the/O Federal/ORGANIZATION Reserve/ORGANIZATION Bank/ORGANIZATION of/ORGANIZATION New/ORGANIZATION York/ORGANIZATION and/O led/O by/O Timothy/PERSON R./PERSON Geithner/PERSON ,/O the/O president/O of/O the/O New/ORGANIZATION York/ORGANIZATION Fed/ORGANIZATION ,/O and/O Treasury/ORGANIZATION Secretary/O Henry/PERSON M./PERSON Paulson/PERSON Jr./PERSON

Note. In `StanfordNERWebService.java` a web interface is used instead of running `StanfordNER.java` to speed up running time.
##### Thoughts

I would think IceNER is compatible for this part.

##### Thoughts
#### QuestionClassifierWithStanfordClassifier.java
Stanford classifier, see [www-nlp.stanford.edu/software/classifier.shtml](http://www-nlp.stanford.edu/software/classifier.shtml). Details on usage in `README.txt` and wiki [www-nlp.stanford.edu/wiki/Software/Classifier](http://www-nlp.stanford.edu/wiki/Software/Classifier)

The model used for `QA-REF` has been trained over hand labeled 2000 question from http://l2r.cs.uiuc.edu/~cogcomp/Data/QA/QC/. The link is broken. The labelling suggested by Li and Roth in 2001 is used. 

**Missing demo**



##### Thoughts

* How about making a simple question classifier that only detects simple questions like Hvar, Hver, Hvenær etc.?

#### StanfordPOSTagger.java
Stanford Part-of-speech tagger, demo available at [nlp.stanford.edu](http://nlp.stanford.edu:8080/parser/index.jsp). The trained `bidirectional-wsj-0-18.tagger` tagger is used. Called in QA-REF from classes

* `sg/edu/nus/wing/qanus/stock/ar/BasicIRBasedStrategyUsingWebAsCorpus.java` line 63
* `sg/edu/nus/wing/qanus/stock/ar/featurescoring/FeatureScoringStrategy.java` line 112
* `sg/edu/nus/wing/qanus/stock/ibp/Controller.java` line 62
* `sg/edu/nus/wing/qanus/stock/qp/Controller.java` line 92

Sample input

    My dog also likes eating sausage.

results in output
Tagging
    
    My/PRP$ dog/NN also/RB likes/VBZ eating/VBG sausage/NN ./.

Parse

    (ROOT
      (S
        (NP (PRP$ My) (NN dog))
        (ADVP (RB also))
        (VP (VBZ likes)
          (S
            (VP (VBG eating)
              (NP (NN sausage)))))
        (. .)))

Typed dependencies

    poss(dog-2, My-1)
    nsubj(likes-4, dog-2)
    advmod(likes-4, also-3)

root(ROOT-0, likes-4)
    
    xcomp(likes-4, eating-5)
    dobj(eating-5, sausage-6)

Typed dependencies, collapsed

    poss(dog-2, My-1)
    nsubj(likes-4, dog-2)
    advmod(likes-4, also-3)
    root(ROOT-0, likes-4)
    xcomp(likes-4, eating-5)
    dobj(eating-5, sausage-6)
##### Thoughts

I would think IceTagger is compatible for the parsing part. Are there any dependency tools in `IceNLP`? Making a quick search through the `QA-REF` source files I could not find any code that makes use of the dependencies. However, since the `QA-REF` classes are only wrappers it might be that the dependencies are being used in the background by the Stanford NLP tools.

#### StanfordGrammarParser.java
Stanford parser, same output and demo as for the POSTagger. Uses the `getBestParse()` method which has the following javadoc

    Return the best parse of the sentence most recently parsed.
    This will be from the factored parser, if it was used and it succeeeded
    else from the PCFG if it was used and succeed, else from the dependency
    parser.


##### Thoughts

* Why is the output the same as from POSTagger?

I would think IceTagger is compatible for the parsing part.

#### StopWordsFilter
Implemented in `QA-REF`, used to remove stop words. Stop words are read from file `QANUS/dist/lib/common-english-words.txt` which contains words

    a,able,about,across,after,all,almost,also,am,among,an,and,any,are,as,at,be,because,been,but,by,can,cannot,could,dear,did,do,does,either,else,ever,every,for,from,get,got,had,has,have,he,her,hers,him,his,how,however,i,if,in,into,is,it,its,just,least,let,like,likely,may,me,might,most,must,my,neither,no,nor,not,of,off,often,on,only,or,other,our,own,rather,said,say,says,she,should,since,so,some,than,that,the,their,them,then,there,these,they,this,tis,to,too,twas,us,wants,was,we,were,what,when,where,which,while,who,whom,why,will,with,would,yet,you,your

##### Thoughts

This code is fairly simple, I'm guessing it would be possible to implement something similar for Icelandic.

Question Processing
--------------------
Figure out what the user wants

#### Controller
`IXMLParser`: `TRECQuestionXMLHandler`     
`IRegisterableModule`: `QuestionClassifierWithStanfordClassifier`, `StanfordNER`, `StanfordPOSTagger`


Answer Retrieval
-----------------
The brain of the system

#### Controller
`IXMLParser`: `TREC2007AnnotatedQuestionXMLParser`     
`IRegisterableModule`: `FeatureScoringStrategy`

#### AnswerRetriever
Unlike the other stages, the method `Notify(DataItem a_item):void` makes use of Java synchronisation for concurrent execution.


#### FeatureScore
Responsible for tabulating the scores of candidate passages.

`AddDocument(String docString):void`      
`RetrieveTopDocuments(String query, int n):String[]`   

#### FeatureScoringStrategy
2000 lines of code.

> Serves as a (somewhat bad) example of how new AR strategies could be added to QANUS.

Contains hardcoded patterns for retrieving an answer for an english corpus. Main function

`GetAnswerForQuestion(DataItem a_QuestionItem, boolean a_Analysis):DataItem` 

is over 1200 lines. Uses the Li and Roth classification, over 2200 question types.

###### Thoughts

* Queries twice, first to the lucene index to retrive a large number of documents and then queries a `FeatureScorer` for fewer answers.

#### FreebaseQuerier
Used to confirm that 

##### Thoughts

Questions
--------------

* What strategy should we use to retrieve the correct answer?
    * `IAnswererFramework`
* Should we consider getting access to Gettu Betur questions?
    * It might be a great source of factoid questions
* What is the IFD corpus?
* Which book in bookshelf on Icelandic grammar?
* Are there any available Apache Lucene customizations for Icelandic?
    * Analyser
    * Query parser
* Could I get my name on the list of people? [nlp.cs.ru.is](http://nlp.cs.ru.is/)

##### Notes
* There are missing a few classes from the IceNLPCore source code.
    * My project has errors
* IceNLPCore is built with Java 7, makes it incompatible with java classes compiled with Java 6. It took some 


#### Answered
* What happened to [tungutaekni.is/](http://www.tungutaekni.is/)?
    > Tungutækni was the original translation of "language technology".  However, now we only refer to Máltækni.
* Does there exist a common website containing accessible and standardised information on available language resources and tools for the Nordic languages? 
    * Taken from the paper [Icelandic Language Resources and Technology: Status and Prospects](http://dspace.utlib.ee/dspace/bitstream/handle/10062/9670/Icelandic%20language%20resources.pdf?sequence=1).
    > No, but have a look at META-NORD
    
* Will the final report be written in Icelandic or English?
    > English

* How about translating queries into english?
    * Could be powerful for general purpose questions
    * Answers could be either returned in Icelandic or English
    * Could make use of large knowledge databases already existing in English
        * See [other tools](#tools)
    * Could it be used as a tool for comparison?
    * Information source would be entirely in English
    * Multiple options available
        * Translate query directly
        * Process query in icelandic and then translate
    > It is an interesting idea. This would limit the information base to an english corpus. Also, the answer retrieved would be in english. 
    
* What would be a good first approach to get `QA-REF` working with `IceNLP`?
    > Small iterations, make the pipeline work
    
* What should the new QA system be called?
    * IceSYS?
    * Question-Answering Language-Independent System, QA-LIS?
    > IceQA

* Would it be ok if I set up a project on Github or Bitbucket.org?
    > Yes
    
* What a language independent system involves?
    > QANUS seems to be fairly language independent. Make no unnecessary restrictions on any specific language
* Is there a wiki for ICLT
    > No, but there is a user guide
    
* How should we prepare the Icelandic Science Web corpus?
    * Pick paragraphs/statements at random for testing
    * Remove html, lists etc. [example](https://visindavefur.hi.is/svar.php?id=25863)
    > Remove all html, begin with simple questions
    
* Any hints on understanding the Stanford NLP tools better? Quickly.
    * Coursera Course
    * Speech and language processing
    * Online demos
* Any good hints on reading research papers in general?
    * Read the abstract
    * Think of a paper much like a dialogue, a platform to test an hypothesis and publish your results. A paper does not necessary have be any snilld.
* Is there a geospatial location database available for Icelandic?
    * [www.geonames.org](http://www.geonames.org/)
* Does `IceNLP` make use of any Stanford NLP tools?
    * Reading [www-nlp.stanford.edu/software/lex-parser.shtml](http://www-nlp.stanford.edu/software/lex-parser.shtml) I see they mention that a few countries have adopted the Stanford parser to parse text from their own languages, such as for Chinese, German, Arabic and more.
    > No, but unsuccessful attempts have been made in the past at training the tools on Icelandic.
* What does Combitagger do?
    * Taken from [combitagger.sourceforge.net/](http://combitagger.sourceforge.net/)
    > The main purpose of CombiTagger is to read ﬁles generated by individual PoS taggers and use them to develop and evaluate combined taggers according to a given combination algorithm.

TODOs
------
* Understand better
    * [  ] `framework.commons`
        * [  ] `IAnalyzable`
        * [  ] `IAnalyzableController`
        * [  ] `IEvaluationMetric`
    * [  ] `stock.ar.er`
        * [  ] `AnalysisInfoSourceReader`
        * [  ] `FactoidPipelineErrorAnalyzer`
        * [  ] `StageStatistics`
    * [  ] `framework.eval.AnswerChecker.java`
    * [  ] JRE vs. JDK
* [  ] Implement function to split sentences, not just `split("\\.")`
* [  ] Characters dissappearing "íbú Egyptalan"
* [  ] IceQA Server in Play!
* [  ] jflex generate code



### Done

* [ x ] Apache Lucene demo
* [ x ] Talk to Egill about Gettu betur
* [ x ] IceNER ITextprocessing module
* [ x ] naming conventions `m_XXX`, `l_XXX` etc.
* [ x ] `framework.commons`
    * [ x ] `IStrategyModule`
* [ x ] `framework.commons.IXMLDataReceipient`
    * [ x ] `framework.ar.AnswerRetriever.java`
    * [ x ] `framework.ibp.InformationBaseEngine.java`
    * [ x ] `framework.qp.QuestionProcessor.java`
* [ x ] `framework.commons.IXMLParser`
* [ x ] `stock.ibp.AQUAINT2XMLHandler`
* uml images
    * [ x ] Framework
    * [ x ] Stock
    * [ x ] ibp
    * [ x ] qp
    * [ x ] ar
    * [ x ] eval
* [ x ] Make icelandic web text data compatible with `QA-REF`'s `stock.ibp.AQUAINT2XMLHandler`
    * Write python script which converts any xml document containing a list of items and outputs an `AQUAINT-2` compatible xml file.
* Learn
    * [ x ] [xml-coreutils](http://xml-coreutils.sourceforge.net/xml-coreutils_man.html)
    * [ x ] [The ElementTree XML API](http://docs.python.org/2/library/    xml.etree.elementtree.html)        



Interesting papers
--------------

* Hirschman, Lynette, and Robert Gaizauskas. “Natural Language Question Answering: The View from Here.” Natural Language Engineering 7, no. 4 (2001): 275–300. 
    * Cited from Ng, Jun-Ping, and Min-Yen Kan. QANUS: An Open-source Question-Answering Platform, 2010. http://aye.comp.nus.edu.sg/~junping/docs/qanus.pdf.
* Lin, Jimmy, and Boris Katz. “Question Answering from the Web Using Knowledge Annotation and Knowledge Mining Techniques.” In Proceedings of the Twelfth International Conference on Information and Knowledge Management, 116–123, 2003. http://dl.acm.org/citation.cfm?id=956886.
    * Author of `ARANEA`


QANUS alternatives
--------------
* Interesting suggestions to a question on [ResearchGate](https://www.researchgate.net/post/Anyone_familiar_with_a_successful_NLP_Machine_Learning_Question_Answering_Knowledge_Extraction_tool)
* `ARANEA`
    * Last released: June 11, 2005. Not under active development anymore
    * Open source licenced

* `QANDA` by MITRE
    * Does not look promising.
    * Written in c
* `OpenEphyra`
    * Seems to have slowed down after 2007-08, at least the for the open source development.
    * Includes closed source .jar files such as javelin

<a id="tools"></a>Other tools
--------------

* [Apache Lucene](http://lucene.apache.org/core/)
    * Used in `QANUS`
* [DBpedia](http://wiki.dbpedia.org/About)
* [Geonames](http://www.geonames.org/)
* [WordNet](http://wordnet.princeton.edu/)
* [FreeBase](http://www.freebase.com/)
    * Used in `QANUS`

Miscellaneous
--------------

* Empty links
    * Click on button `þýða` at [nlp.cs.ru.is/ApertiumISENWeb/](http://nlp.cs.ru.is/ApertiumISENWeb/).
    * Click on `IceNLP` at [www.ru.is/~hrafn/](http://www.ru.is/~hrafn/). A correct link I guess would be [nlp.cs.ru.is/](http://nlp.cs.ru.is/)
* [Rannís Application](https://mail-attachment.googleusercontent.com/attachment/u/0/?ui=2&ik=0d21406b98&view=att&th=13d36a6f8f02c4af&attid=0.1&disp=inline&safe=1&zw&saduie=AG9B_P_NKHVol7rb0_wDar1naH7f&sadet=1370443337462&sads=dOdraKo08ZNOlUyBKWsWv_qYy8M&sadssc=1)

