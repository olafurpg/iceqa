## IceNER problems
The `IceNER` module is horribly implemented and horribly documented.
The API is difficult to use and simply impossible to understand at times.
Javadoc documentation is missing for almost entirely every method. 
Comments are written in English and Icelandic interchangeably.
Variable names often contain spelling mistakes.
Lets look at some examples.

#### Strange class member variables
Lets look at the member variables of the `NameSearcher` class. The comments explain what is strange about the variables.

    // the purpose of this variable is never explained
    // even though it it possible to construct an 
    // instance of the whole class with this
    // variable alone as a parameter
    boolean m_bFormOverFunction;
    
    // We can write to a file with this name
    // without ever initialising its value
    String m_sOutFile;
    
    // HashTable is a raw type, the template parameters <K, V> are missing
    Hashtable m_sNameHash = new Hashtable(); 
    
    // ArrayList is a raw type, the template parameters <K, V> are missing
    ArrayList m_IndexList = new ArrayList();
    
    // We need  the number of lines in
    // a file we want the class to read
    // The provided solution in 
    // icenlp/core/bat/icenlp/iceNER/icener.sh
    //  was to use the bash command `wc -l file`
    int m_nLineCount;
    
    // This variable serves no purpose
    String m_sLine = "";


#### Misspellings
All of the following strings can be found in the `IceNER` module.

* `bGready`
* `tryDifferntCasesBolean`
* `findFromInitilas`
* `isAbrivation`
* `tryDifferntCasesFirstNasme`
* `noEndingBolean`
* `endsWithURBolean`
* `searcer`
* `//geimi hvar nafn birjaði`

#### Strange API
Another class in `IceNER` is `RunNameFinder`, used to provide a clean command line interface to run `IceNER`.
The main method accepts 8 command line arguments, none of which are documented.

    if(nParamCount==8)
            {
                searcher.loadSearcher(args[0], Integer.parseInt(args[1]),args[2], Integer.parseInt(args[3]), args[4], args[6], Integer.parseInt(args[5]));
                bGready = true;
            }
            //With gazett list
            else if(nParamCount==7)
            {
                searcher.loadSearcher(args[0], Integer.parseInt(args[1]),args[2], Integer.parseInt(args[3]), args[4], args[6], Integer.parseInt(args[5]));
            }
            //Greedy
            else if(nParamCount==6)
            {
                searcher.loadSearcher(args[0], Integer.parseInt(args[1]),args[2], Integer.parseInt(args[3]), args[4]);
                bGready = true;
            }
            //Default
            else
            {
                searcher.loadSearcher(args[0], Integer.parseInt(args[1]),args[2], Integer.parseInt(args[3]), args[4]);
            }
            /**/
            //searcer.loadSearcher("C:\\Loka\\Tagged.out",5639,"C:\\Loka\\scan.out", 650, "C:\\Loka\\doc.out", "C:\\Loka\\location.txt",527);
            //searcer.loadSearcher("C:\\Loka\\Tagged.out",1915,"C:\\Loka\\scan.out", 206, "C:\\Loka\\doc.out" );
            //searcer.loadSearcher("C:\\Loka\\Tagged.out",106,"C:\\Loka\\scan.out",650, "C:\\Loka\\doc.out" );
            
#### A lot of code
`NameSearcher` contains 2773 lines of code. 
Opening it makes my IDE run a bit slower.


#### Dangerous array manipulation
IceNER will throw an `ArrayIndexOutOfBoundsExcpetion` for many normal and legal input strings.
For example, running IceNER on the sentence `"Ólafur Ragnar Grímsson er forseti Íslands"` will cause an exception because there is missing a `.` at the end of the line.
Running again after appending a `.` to the input will result in a clean execution.

#### Multiple runs
The module has not been designed to parse text consecutively making it unavoidable to reload the gazette file for every iteration of the module.

#### Bad output
Most importantly, the output from `IceNER` is not accurate.
The module produces both a large number of false negatives as well as true positives.
See it for yourself on any input data not provided by the module itself as example input.


## Conclusion

With this in consideration

>   I suggest that IceNER be removed from IceNLP core.

At least until the module has been rewritten, properly documented and tested with enough input data for reliability.
