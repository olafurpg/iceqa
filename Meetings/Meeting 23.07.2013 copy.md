# IceQA

## Text processing

### IceNER

##### Format
Current format is: 

    Þetta svar hefur einnig birst sem pistill á vef [Stofnunar Árna Magnússonar COMPANY] og er birt hér með góðfúslegu leyfi.

while a better format could be for example 

    Þetta svar hefur einnig birst sem pistill á vef Stofnunar/COMPANY Árna/COMPANY Magnússonar/COMPANY og er birt hér með góðfúslegu leyfi.
    

##### Interface
Currently we are running `IceNER` through a command line interface. 
We execute the script `iceNER.sh` with an input file and output file. 

There must be a better way. 

##### Split sentences
Is there any clever way to split sentences for IceNER tagging.
`ProcessText(String[] a_Sentences):String[]` will execute IceNER through the command line as many times as the length of `a_Sentences`.

### Answer retrieval
An `AnswererFramework` and `IAnswerer` interface has been mostly implemented but there are some details left to be decided upon. 

Instead of loading the answerers in the AR stage we do in the IBP stage.
We add a document to the Lucene index containing answerer subscriptions and classnames, then in during AR we search the index for subscriptions and load up the answerers using `AnswererFactory`. 

##### ? x FeatureScoringStrategy 
QANUS has not been implemented with the `AnswererFramework` in mind. Instead xxx.

##### Questions

* Which methods the `IAnswerer` interface contain?
    * [ x ] `GetSubjectQuestion(POS, NER, Text):String`
    * `GetSubjectQuestion(Map<String, String>):String`
    * `AnswerQuestion(QuestionDataItem):String`
    * [ x ]`AnswerQuestion(FeatureScorer):String`

## Gettu betur
More than 20.000 questions. Could be useful to analyse factoid questions.

>    Ofurskjalið.csv

## IceQuestionClassifier


## TODOs

[  ] Collect questions from the IWS: PERSON, LOCATION, (NUMBER).      
[ x ] Simple QC for questions using pattern matching.    
[  ] IceNER, enable Gazette list.    
[  ] IceNER, extract tags