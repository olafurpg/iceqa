# IceQA

## TODOs
* [ x ] Fix parsing bug
* [ x ] Add answerers controller
* [ x ] Add dev questions
* [ x ] Code `YearAnswerer`
* [ x ] Code `NameAnswerer`
* [ x ] Code `LocationAnswerer`
* [ x ] Passage refactor
* [ x ] Add timestamp to Lucene `AddDoc`
* [ x ] Fix `FeatureSearchTermProximity`
* [ x ] Get cluster access, email Freysteinn
* [ x ] Segmentizer
* [ x ] Number of keywords scorer
* [ x ] Distance scorer
* [ x ] Gazette lists for Names and locations
    * Only answers from our test set

#### Not finished
* [  ] Process all input articles, received error while tagging

    FATAL ERROR - Word to long: FiskarnirFljótiðFolinnHarpanHerkúlesHérinnHjarðmaðurinnHrafninnHrúturinnHvalurinnHöfrungurinnHöggormurinnKassíópeiaKrabbinnLitlibjörnLitlihundurLjóniðMannfákurinnMeyjanNaðurvaldiNautiðNorðurkórónanÓríonPegasusPerseifurSefeusSporðdrekinnSteingeitinStórihun

from article [Hver eru stjörnumerki Ptólemaíosar?](http://visindavefur.is/svar.php?id=55659)


* [ ] Parse HTML correctly
* [ ] Fix `IceNLPFacade`
    * preferably extending the API instead of executing the bash script

## Performance
By running IceQA to only answer articles with 100 question the precision of the system is 41%.
When only evaluating `YearAnswerer` the system performs with an increased precision rate of approximately 62%, or 27 out of 43 questions answered correctly.

## Questions
* What are the next steps?