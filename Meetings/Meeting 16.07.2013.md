# IceQA

## Text processing

### IceNER

##### Format
Current format is: 

    Þetta svar hefur einnig birst sem pistill á vef [Stofnunar Árna Magnússonar COMPANY] og er birt hér með góðfúslegu leyfi.

while a better format could be for example 

    Þetta svar hefur einnig birst sem pistill á vef Stofnunar/COMPANY Árna/COMPANY Magnússonar/COMPANY og er birt hér með góðfúslegu leyfi.
    

##### Interface
Currently we are running `IceNER` through a command line interface. 
We execute the script `iceNER.sh` with an input file and output file. 

There must be a better way. 

##### Split sentences
Is there any clever way to split sentences for IceNER tagging.
`ProcessText(String[] a_Sentences):String[]` will execute IceNER through the command line as many times as the length of `a_Sentences`.

### Answer retrieval
An `AnswererFramework` and `IAnswerer` interface has been mostly implemented but there are some details left to be decided upon. 

Instead of loading the answerers in the AR stage we do in the IBP stage.
We add a document to the Lucene index containing answerer subscriptions and classnames, then in during AR we search the index for subscriptions and load up the answerers using `AnswererFactory`. 

##### ? x FeatureScoringStrategy 
QANUS has not been implemented with the `AnswererFramework` in mind. Instead xxx.

##### Questions

* Which methods the `IAnswerer` interface contain?
    * [ x ] `GetSubjectQuestion(POS, NER, Text):String`
    * `GetSubjectQuestion(Map<String, String>):String`
    * `AnswerQuestion(QuestionDataItem):String`
    * [ x ]`AnswerQuestion(FeatureScorer):String`

## Gettu betur
More than 20.000 questions. Could be useful to analyse factoid questions.

>    Ofurskjalið.csv

## IceQuestionClassifier

## Collect questions
#### Hvað er góð spurning?
**Grein:**
Íslendingar hafa átt þátttakendur á 18 Ólympíuleikum (en hér er aðeins átt við sumarólympíuleika). Fyrst árið 1912 en þá vitanlega undir fána Dana, því næst árið 1936, þá fyrst sem fullvalda þjóð, og allar götur síðan. Þess ber að geta að engir Ólympíuleikar fóru fram árin 1916, 1940 og 1944 vegna stríðsátaka í heiminum.


Íslendingar hafa unnið til fernra verðlauna á Ólympíuleikum.

Á þessum 18 Ólympíuleikum hafa Íslendingar unnið til fernra verðlauna, tvennra silfurverðlauna og tvennra bronsverðlauna:
Árið 1956 voru leikarnir haldnir í Melbourne í Ástralíu en þá vann Vilhjálmur Einarsson silfurverðlaun í þrístökki.
Næstur var Bjarni Friðriksson sem vann til bronsverðlauna í júdó í Los Angeles í Bandaríkjunum árið 1984.
Árið 2000 í Sidney í Ástralíu vann Vala Flosadóttir til bronsverðlauna í stangarstökki.
Árið 2008 í Peking í Kína var svo komið að karlalandsliðinu í handknattleik en þeir unnu til silfurverðlauna. Í liðinu voru: Alexander Petersson, Arnór Atlason, Ásgeir Örn Hallgrímsson, Björgvin Páll Gústavsson, Guðjón Valur Sigurðsson, Hreiðar Levy Guðmundsson, Ingimundur Ingimundarson, Logi Geirsson, Ólafur Stefánsson, Róbert Gunnarsson, Sigfús Sigurðsson, Snorri Steinn Guðjónsson, Sturla Ásgeirsson og Sverre Andreas Jakobsson.Íslendingar hafa átt þátttakendur á 18 Ólympíuleikum (en hér er aðeins átt við sumarólympíuleika). Fyrst árið 1912 en þá vitanlega undir fána Dana, því næst árið 1936, þá fyrst sem fullvalda þjóð, og allar götur síðan. Þess ber að geta að engir Ólympíuleikar fóru fram árin 1916, 1940 og 1944 vegna stríðsátaka í heiminum.


Íslendingar hafa unnið til fernra verðlauna á Ólympíuleikum.

Á þessum 18 Ólympíuleikum hafa Íslendingar unnið til fernra verðlauna, tvennra silfurverðlauna og tvennra bronsverðlauna:
Árið 1956 voru leikarnir haldnir í Melbourne í Ástralíu en þá vann Vilhjálmur Einarsson silfurverðlaun í þrístökki.
Næstur var Bjarni Friðriksson sem vann til bronsverðlauna í júdó í Los Angeles í Bandaríkjunum árið 1984.
Árið 2000 í Sidney í Ástralíu vann Vala Flosadóttir til bronsverðlauna í stangarstökki.
Árið 2008 í Peking í Kína var svo komið að karlalandsliðinu í handknattleik en þeir unnu til silfurverðlauna. Í liðinu voru: Alexander Petersson, Arnór Atlason, Ásgeir Örn Hallgrímsson, Björgvin Páll Gústavsson, Guðjón Valur Sigurðsson, Hreiðar Levy Guðmundsson, Ingimundur Ingimundarson, Logi Geirsson, Ólafur Stefánsson, Róbert Gunnarsson, Sigfús Sigurðsson, Snorri Steinn Guðjónsson, Sturla Ásgeirsson og Sverre Andreas Jakobsson.


## TODOs

[  ] Collect questions from the IWS: PERSON, LOCATION, (NUMBER).      
[ x ] Simple QC for questions using pattern matching.    
[  ] IceNER, enable Gazette list.    
[  ] IceNER, extract tags